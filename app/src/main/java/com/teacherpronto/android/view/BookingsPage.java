package com.teacherpronto.android.view;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

import com.common.callback.NetCallBackMini;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.markmao.pulltorefresh.widget.XListView;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;
import com.teacherpronto.android.activity.JobDetailActivity;
import com.teacherpronto.android.activity.MainActivity;
import com.teacherpronto.android.adapter.JobListAdapter;
import com.teacherpronto.android.bean.Job;
import com.teacherpronto.android.net.NetInterface;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by clia5292 on 2016/4/25.
 */
public class BookingsPage extends BasePage {

    private Animation animationFadeIn;
    private Animation animationFadeOut;

    private XListView listView;
    private RotateAnimation loadingAnimation;
    private JobListAdapter jobListAdapter;
    private ArrayList<Job> arrayListJobs;

    private String filterType = "0";
    private int pageSize = 10;


    private int startpage = 1;

    private int currentPage = startpage;
    private int totalCount = 0;

    private short SHORT_GO_DETAIL = 100;
    private Job jobCurrentClick = null;


//    private View viewRefreshEmpty;
//    private TextView textViewEmptyTitle;

    public BookingsPage(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.rootView = View.inflate(mainActivity, R.layout.page_bookings, null);

        animationFadeIn = AnimationUtils.loadAnimation(mainActivity, R.anim.abc_fade_in);
        animationFadeOut = AnimationUtils.loadAnimation(mainActivity, R.anim.abc_fade_out);

        animationFadeIn.setDuration(200);
        animationFadeOut.setDuration(200);
        //===============

        initView();
    }

    @Override
    public void onShow() {

        if (ListUtiles.isEmpty(arrayListJobs) || needRefresh == true) {
            currentPage = startpage;
            listView.autoRefresh();
//            reqeustJobList(true, filterType, false);

            needRefresh = false;
        }


    }

    private void initView() {
        listView = (XListView) findViewById(R.id.listView);
        loadingAnimation = (RotateAnimation) AnimationUtils.loadAnimation(mainActivity, R.anim.rotating);
        LinearInterpolator lir = new LinearInterpolator();
        loadingAnimation.setInterpolator(lir);
        arrayListJobs = new ArrayList<>();
        jobListAdapter = new JobListAdapter(mainActivity, arrayListJobs);
        listView.setAdapter(jobListAdapter);


//        viewRefreshEmpty = findViewById(R.id.viewRefreshEmpty);
//        textViewEmptyTitle = findTextViewById(R.id.textViewEmptyTitle);
//        viewRefreshEmpty.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                listView.autoRefresh();
//                hideEmptyView();
//            }
//        });


        this.listView.setPullRefreshEnable(true);
        this.listView.setPullLoadEnable(true);
//        this.listView.getmFooterView().hide();
        this.listView.getFooterView().setVisibility(View.GONE);


        jobListAdapter.setOnJobItemClickListenerl(new JobListAdapter.OnJobItemClickListener() {
            @Override
            public void onClickItem(Job job, int index) {
                App.getApp().putTemPObject("job", job);
                jobCurrentClick = job;
                Tool.startActivityForResult(mainActivity, JobDetailActivity.class, SHORT_GO_DETAIL);
            }

            @Override
            public void onAcceptClick(final Job job, int index) {


                DialogUtils.showConfirmDialog(mainActivity, "", "Are you sure you want to accept this job?", "Confirm", "Cancel", new DialogCallBackListener() {
                    @Override
                    public void onDone(boolean yesOrNo) {
                        if (yesOrNo) {
                            aceptOrDelayJob(job, true);
                        }
                    }
                });


            }

            @Override
            public void onDeclineClick(final Job job, int index) {


                DialogUtils.showConfirmDialog(mainActivity, "", "Are you sure you want to decline this job?", "Confirm", "Cancel", new DialogCallBackListener() {
                    @Override
                    public void onDone(final boolean yesOrNo) {
                        if (yesOrNo) {
                            aceptOrDelayJob(job, false);
                        }
                    }
                });


            }
        });


        this.listView.setXListViewListener(new XListView.IXListViewListener() {
            @Override
            public void onRefresh() {
                currentPage = startpage;
                reqeustJobList(true, filterType, false);
            }

            @Override
            public void onLoadMore() {
                currentPage++;
                if (ListUtiles.getListSize(arrayListJobs) < totalCount) {
                    reqeustJobList(false, filterType, false);
                }

            }
        });


    }


//    private void showEmptyView() {
//
//        viewRefreshEmpty.setVisibility(View.VISIBLE);
//    }
//
//    private void hideEmptyView() {
//        viewRefreshEmpty.setVisibility(View.GONE);
//    }


    public void aceptOrDelayJob(final Job job, final boolean isAccept) {

        mainActivity.acceptDelayJob(mainActivity, true, job.getJobId(), isAccept, new NetCallBackMini() {
            @Override
            public void onFinish(NetResult netResult) {

                if (null != netResult) {

                    if (netResult.isValidateError()) {

//                        {status": 401, "message": "Unauthorized"}    token认证失败
//                        {"status": 401, message": "invalid_credentials"}   login用户名密码认证失败
//                        {"status": 422, "message": "validation_failed", "error": "{validation failed message here}"}

                        App.logout2Login(mainActivity, netResult.getCode() + "," + netResult.getMessage());
                        return;


                    } else if (netResult.isOk()) {

                        Tool.showMessageDialog(netResult.getMessage(), mainActivity);
                        if (isAccept) {
                            job.setType(Job.STATUS_ACCEPT);
                        } else {
                            job.setType(Job.STATUS_DECLINE);
                        }
                        jobListAdapter.notifyDataSetChanged();

                    } else {
                        Tool.showMessageDialog(netResult.getMessage(), mainActivity);

                    }

                } else {
                    Tool.showMessageDialog(R.string.error_net, mainActivity);
                }

            }
        });


    }


    public void onFilterTypeMenuClick(String filterType) {
        this.filterType = filterType;
        reqeustJobList(true, this.filterType, true);

    }

    private void reqeustJobList(final boolean isRefresh, final String filterType, final boolean showDialog) {
        mainActivity.reSetTask();

        mainActivity.baseTask = new BaseTask(mainActivity, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }

//                hideEmptyView();
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {

                    JSONObject requestObj = new JSONObject();

                    requestObj.put("filter_type", filterType);
                    requestObj.put("page_size", pageSize);
                    requestObj.put("page", currentPage);

//                    App.getApp().setToken("11");

                    netResult = NetInterface.getJobList(mainActivity, requestObj);


                } catch (Exception e) {
                    LogUtil.e(App.tag, "request job list error:" + e.getLocalizedMessage());

                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (showDialog) {
                    baseTask.hideDialogSelf();
                }


                stopXlist(listView);

                if (null != result) {


                    if (result.isValidateError()) {

//                        {status": 401, "message": "Unauthorized"}    token认证失败
//                        {"status": 401, message": "invalid_credentials"}   login用户名密码认证失败
//                        {"status": 422, "message": "validation_failed", "error": "{validation failed message here}"}

                        App.logout2Login(mainActivity, result.getCode() + "," + result.getMessage());
                        return;


                    } else if (result.isOk()) {

                        ArrayList<Job> tempLists = (ArrayList<Job>) result.getData()[0];
                        totalCount = (Integer) result.getData()[1];

                        if (isRefresh) {
                            arrayListJobs.clear();
                            currentPage = startpage;
                            arrayListJobs.addAll(tempLists);

                        } else {
                            arrayListJobs.addAll(tempLists);
                        }
                        jobListAdapter.notifyDataSetChanged();


                        if (ListUtiles.isAllEmpty(tempLists, arrayListJobs)) {
//                            showEmptyView();
                        } else if (ListUtiles.isEmpty(tempLists) && isRefresh != false) {
//                            Tool.showMessageDialog("No more data", mainActivity);
                            Tool.ToastShow(mainActivity, "No more data");
                        } else if (ListUtiles.isEmpty(tempLists) && isRefresh == true) {
//                            Tool.showMessageDialog("No data", mainActivity);
//                            showEmptyView();
                        }

                        if (false == isRefresh && ListUtiles.isEmpty(tempLists)) {
                            currentPage--;
                        }
                        LogUtil.e(App.tag, "total job size:" + jobListAdapter.getCount());
                    } else {
                        Tool.showMessageDialog(result.getMessage(), mainActivity);
                        if (isRefresh == false) {
                            currentPage--;
                        }
                    }

                } else {
                    Tool.showMessageDialog(R.string.error_net, mainActivity);
                    if (isRefresh == false) {
                        currentPage--;
                    }
                }

                dealFooter(arrayListJobs, totalCount, listView);

            }
        });

        mainActivity.baseTask.execute(new HashMap<String, String>());


    }


    protected void stopXlist(XListView xlist) {
        xlist.stopRefreshLoadUI();
    }

    protected void dealFooter(List arrayList, int totalNumber, XListView listViewNannies) {
        try {
            if (arrayList.size() >= totalNumber) {
                listViewNannies.getFooterView().setVisibility(View.GONE);

            } else {
                listViewNannies.getFooterView().setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        LogUtil.i(App.tag, "data size:" + arrayList.size() + " total:" + totalNumber);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SHORT_GO_DETAIL && resultCode == Activity.RESULT_OK) {

            String job_status = (String) App.getApp().getTempObject("job_status");

            if (null != jobCurrentClick && !StringUtils.isEmpty(job_status)) {
                jobCurrentClick.setType(job_status);
                jobListAdapter.notifyDataSetChanged();
            }

        }


    }
}
