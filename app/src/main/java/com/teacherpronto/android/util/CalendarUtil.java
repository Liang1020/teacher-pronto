package com.teacherpronto.android.util;

import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.teacherpronto.android.App;
import com.teacherpronto.android.bean.AvailableDate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by clia5292 on 2016/5/2.
 */
public class CalendarUtil {

    public static int getMonthDays(int year, int month) {
        if (month > 12) {
            month = 1;
            year += 1;
        } else if (month < 1) {
            month = 12;
            year -= 1;
        }
        int[] arr = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        int days = 0;

        if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
            arr[1] = 29;
        }

        try {
            days = arr[month - 1];
        } catch (Exception e) {
            e.getStackTrace();
        }

        return days;
    }

    public static int getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    public static int getMonth() {
        return Calendar.getInstance().get(Calendar.MONTH) + 1;
    }

    public static int getCurrentMonthDay() {
        return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    }

    public static int getWeekDay() {
        return Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
    }

    public static String getMonthString(int month) {

        switch (month - 1) {
            case 0:
                return "January";
            case 1:
                return "February";
            case 2:
                return "March";
            case 3:
                return "April";
            case 4:
                return "May";
            case 5:
                return "June";
            case 6:
                return "July";
            case 7:
                return "August";
            case 8:
                return "September";
            case 9:
                return "October";
            case 10:
                return "November";
            case 11:
                return "December";
            default:
                return null;
        }
    }

    public static int CompareDate(int day, int month, int year){
        Calendar cal = Calendar.getInstance();
        cal.setTime(getDateFromString(year, month, day));
        LogUtil.i(App.tag, cal.getTime().toString());
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());
        now.set(Calendar.MILLISECOND, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.HOUR_OF_DAY, 0);

        LogUtil.i(App.tag, now.getTime().toString());
        return cal.compareTo(now);// -1 cal early| 1 cal late
    }

    public static String getWeekString(int day, int month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getDateFromString(year, month, day));
        int week_index = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (week_index < 0) {
            week_index = 0;
        }
        String daystr = null;
        switch (week_index) {
            case 1:
                daystr = "Monday";
                break;
            case 2:
                daystr = "Tuesday";
                break;
            case 3:
                daystr = "Wednesday";
                break;
            case 4:
                daystr = "Thursday";
                break;
            case 5:
                daystr = "Friday";
                break;
            default:
                break;
        }
        return daystr;
    }

    public static int getWeek(int day, int month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getDateFromString(year, month, day));
        int week_index = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (week_index < 0) {
            week_index = 0;
        }
        return week_index;
    }

    public static AvailableDate getNextSunday() {

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 7 - getWeekDay()+1);
        AvailableDate date = new AvailableDate(c.get(Calendar.YEAR),
                c.get(Calendar.MONTH)+1, c.get(Calendar.DAY_OF_MONTH));
        return date;
    }

    public static int[] getWeekSunday(int year, int month, int day, int pervious) {
        int[] time = new int[3];
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        c.add(Calendar.DAY_OF_MONTH, pervious);
        time[0] = c.get(Calendar.YEAR);
        time[1] = c.get(Calendar.MONTH )+1;
        time[2] = c.get(Calendar.DAY_OF_MONTH);
        return time;

    }

    public static int getWeekDayFromDate(int year, int month) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getDateFromString(year, month));
        int week_index = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (week_index < 0) {
            week_index = 0;
        }
        return week_index;
    }

    public static Date getDateFromString(int year, int month) {
        String dateString = year + "-" + (month > 9 ? month : ("0" + month))
                + "-01";
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
        return date;
    }

    public static Date getDateFromString(int year, int month, int day){
        String dateString = year + "-" + (month > 9 ? month : ("0" + month)) + "-" + (day > 9 ? day : ("0" + day));
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
        return date;
    }

    public static String monthFormat(String monthIn) {
        if (StringUtils.isEmpty(monthIn)) {
            return "01";
        } else {
            if (monthIn.length() == 1) {
                return "0" + monthIn;
            } else {
                return monthIn;
            }
        }
    }

    public static String dayFormat(String day) {
        if (StringUtils.isEmpty(day)) {
            return "01";
        } else {
            if (day.length() == 1) {
                return "0" + day;
            } else {
                return day;
            }
        }
    }

    public static boolean isToday(AvailableDate date){
        return(date.getYear() == CalendarUtil.getYear() &&
                date.getMonth() == CalendarUtil.getMonth()
                && date.getDay() == CalendarUtil.getCurrentMonthDay());
    }

    public static boolean isCurrentMonth(AvailableDate date){
        return(date.getYear() == CalendarUtil.getYear() &&
                date.getMonth() == CalendarUtil.getMonth());
    }
}
