package com.teacherpronto.android.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.teacherpronto.android.R;

/**
 * Created by clia on 2016/5/9.
 */
public class DialogRepeat extends MyBasePicker implements View.OnClickListener{

    private Context context;
    private int tag;
    private LinearLayout linearLayout;
    private Button btnCancel;
    private Button btnDone;
    private TextView txtRepeat;
    private ListView mListView;
    private ArrayAdapter<CharSequence> arrayAdapter;

    private boolean isExtended = false;
    private Drawable arrowRight, arrowDown;
    private String repeatTag ="None";
    private OnDoneClickListener onDoneClickListener;

    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, 0, 3);
    LinearLayout.LayoutParams lpE = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, 0, 6);

    public DialogRepeat(Context context, int tag){
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_repeat, contentContainer);
        this.context = context;
        this.tag = tag;

        arrowRight = context.getResources().getDrawable(R.drawable.icon_arrow_right);
        arrowDown = context.getResources().getDrawable(R.drawable.icon_arrow_down);

        arrowRight.setBounds(0, 0, arrowRight.getMinimumWidth(), arrowRight.getMinimumHeight());
        arrowDown.setBounds(0, 0, arrowDown.getMinimumWidth(), arrowDown.getMinimumHeight());

        initView();
    }

    private void initView(){
        linearLayout = (LinearLayout) findViewById(R.id.linearLayoutRepeat);
        lp.setMargins(100, 0, 100, 0);
        lpE.setMargins(100, 0, 100, 0);

        mListView = (ListView) findViewById(R.id.listViewRepeat);
        arrayAdapter = ArrayAdapter.createFromResource(context, R.array.repeat, R.layout.custom_listview_item);
        mListView.setAdapter(arrayAdapter);

        btnCancel = (Button) findViewById(R.id.btnRepeatCancel);
        btnDone = (Button) findViewById(R.id.btnRepeatDone);
        txtRepeat = (TextView) findViewById(R.id.textVeiwRepeat);

        btnCancel.setOnClickListener(this);
        btnDone.setOnClickListener(this);
        txtRepeat.setOnClickListener(this);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                switch (position){
                    case 0:
                        repeatTag = "None";
                        txtRepeat.setText("None");break;
                    case 1:
                        repeatTag = "Weekly";
                        txtRepeat.setText("Weekly");break;
                    case 2:
                        repeatTag = "Fornightly";
                        txtRepeat.setText("Fornightly");break;
                    case 3:
                        repeatTag = "Monthly";
                        txtRepeat.setText("Monthly");break;
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnRepeatDone:
                if (null != onDoneClickListener) {
                    onDoneClickListener.onDoneClick(repeatTag);
                }
                dismiss();
            case R.id.btnRepeatCancel:
                dismiss();
            case R.id.textVeiwRepeat:
                if(isExtended == false){
                    txtRepeat.setCompoundDrawables(null, null, arrowDown, null);
                    isExtended = true;
                    linearLayout.setLayoutParams(lpE);
                    mListView.setVisibility(View.VISIBLE);
                }else{
                    txtRepeat.setCompoundDrawables(null, null, arrowRight, null);
                    isExtended = false;
                    linearLayout.setLayoutParams(lp);
                    mListView.setVisibility(View.GONE);
                }
        }
    }


    public static interface OnDoneClickListener {

        public void onDoneClick(String repeatTag);
    }

    public OnDoneClickListener getOnDoneClickListener() {
        return onDoneClickListener;
    }

    public void setOnDoneClickListener(OnDoneClickListener onDoneClickListener) {
        this.onDoneClickListener = onDoneClickListener;
    }
}
