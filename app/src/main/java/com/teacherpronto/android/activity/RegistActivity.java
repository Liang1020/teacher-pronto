package com.teacherpronto.android.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.util.ValidateTool;
import com.onesignal.OneSignal;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;
import com.teacherpronto.android.bean.User;
import com.teacherpronto.android.net.NetInterface;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class RegistActivity extends BaseTeacherActivity {


    @InjectView(R.id.textViewTermsConditions)
    TextView textViewTermsConditions;

    @InjectView(R.id.edit_email)
    EditText editTextEmail;

    @InjectView(R.id.edit_first_name)
    EditText editTextFirstName;

    @InjectView(R.id.edit_last_name)
    EditText editTextLastName;

    @InjectView(R.id.edit_pwd)
    EditText editTextPwd;

    @InjectView(R.id.edit_pwd_confirm)
    EditText editTextPwdConfirm;

    @InjectView(R.id.checkbox_terms)
    CheckBox checkbox_terms;

    private CheckBox checkBoxTerms;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist);
        ButterKnife.inject(this);
        App.getApp().addActivity(this);

        checkBoxTerms = (CheckBox) findViewById(R.id.checkbox_terms);

        Spanned htmlText = Html.fromHtml("I accept the <b>Terms & Conditions</b>");
        textViewTermsConditions.setText(htmlText);

    }

    private boolean checkValue() {

        String email = getInput(editTextEmail);

        if (!ValidateTool.checkEmail(email)) {
            Tool.showMessageDialog("Email format error!", this);
            return false;
        }

        String firstName = getInput(editTextFirstName);
        String lastName = getInput(editTextLastName);
        String pwd = getInput(editTextPwd);

        if (StringUtils.isEmpty(firstName)) {
            Tool.showMessageDialog(getResources().getString(R.string.password_error_notify), this);
            return false;
        }

        if (StringUtils.isEmpty(lastName)) {
            Tool.showMessageDialog(getResources().getString(R.string.password_error_notify), this);
            return false;
        }

        if (StringUtils.isEmpty(pwd)) {
            Tool.showMessageDialog(getResources().getString(R.string.password_error_notify), this);
            return false;
        }

        String conpwd = getInput(editTextPwdConfirm);

        if (!conpwd.equals(pwd)) {

            Tool.showMessageDialog("Confirm password error!", this);

            return false;
        }

        if (!checkbox_terms.isChecked()) {
            Tool.showMessageDialog("Please accept the Terms & Conditions first.", this);

            return false;
        }


        return true;
    }

    private void requestRegister(final JSONObject jsonObject) {
        reSetTask();

        App.getApp().setToken("");

        baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    netResult = NetInterface.register(baseTask.weakReferenceContext.get(), jsonObject);

                } catch (Exception e) {

                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {

                        User user = (User) (result.getData()[0]);

                        App.getApp().setUser(user);

                        requestLogin();

//                        Tool.showMessageDialog(result.getMessage(), (Activity) baseTask.weakReferenceContext.get());
                    } else {
                        Tool.showMessageDialog(result.getMessage(), (Activity) baseTask.weakReferenceContext.get());
                    }
                } else {
                    Tool.showMessageDialog("Net error", (Activity) baseTask.weakReferenceContext.get());
                }

            }
        }, this);

        baseTask.execute(new HashMap<String, String>());

    }


    @OnClick(R.id.textViewTermsConditions)
    public void onClickTerms() {

        Tool.startUrl(RegistActivity.this, "https://www.teacherpronto.com.au/terms-and-conditions");

    }

    private void requestLogin() {

        reSetTask();

        baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);

            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("email", getInput(editTextEmail));
                    jsonObject.put("password", getInput(editTextPwd));


                    SharePersistent.setObjectValue(RegistActivity.this, App.KEY_EMAIL, getInput(editTextEmail));
                    SharePersistent.setObjectValue(RegistActivity.this, KEY_EMAIL_PWD, getInput(editTextPwd));

                    netResult = NetInterface.login(RegistActivity.this, jsonObject);
                } catch (Exception e) {
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (!isFinishing()) {
                    if (null != result) {
                        if (result.isOk()) {

                            //======send onesignal tag====
                            User user = (User) result.getData()[0];
                            OneSignal.sendTag("userid", user.getUser_id());
                            //======send onesignal tag end====

                            sendVerificationQuality(RegistActivity.this);

                            Tool.startActivity(RegistActivity.this, VerificationActivity.class);

//                            Tool.showMessageDialog(result.getMessage(), RegistActivity.this, new DialogCallBackListener() {
//                                @Override
//                                public void onDone(boolean yesOrNo) {
//                                    Tool.startActivity(RegistActivity.this, VerificationActivity.class);
//                                }
//                            });


                        } else {
                            Tool.showMessageDialog(result.getMessage(), RegistActivity.this);
                        }
                    } else {
                        Tool.showMessageDialog("Net error", RegistActivity.this);
                    }
                }
            }
        }, this);

        baseTask.execute(new HashMap<String, String>());

    }

    @OnClick(R.id.buttonSignUp)
    public void onSignUpClick() {
        if (checkValue()) {

            try {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("email", getInput(editTextEmail));
                jsonObject.put("password", getInput(editTextPwdConfirm));
                jsonObject.put("first_name", getInput(editTextFirstName));
                jsonObject.put("last_name", getInput(editTextLastName));

                requestRegister(jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


    @OnClick(R.id.textViewBack2Login)
    public void onBackclick() {
        finish();
    }
}
