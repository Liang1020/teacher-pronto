package com.teacherpronto.android.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.common.callback.NetCallBackMini;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.MyAnimationUtils;
import com.common.util.Tool;
import com.onesignal.OneSignal;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;
import com.teacherpronto.android.adapter.FilterAdapter;
import com.teacherpronto.android.dialog.ConfirmDialog;
import com.teacherpronto.android.fragment.SlideMenuFragment;
import com.teacherpronto.android.net.NetInterface;
import com.teacherpronto.android.view.BasePage;
import com.teacherpronto.android.view.BookingsPage;
import com.teacherpronto.android.view.NotifyDialog;
import com.teacherpronto.android.view.ProfilePage;
import com.teacherpronto.android.view.SetAvailablePage;
import com.teacherpronto.android.view.SettingPage;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MainActivity extends BaseSlideActivity {

    @InjectView(R.id.textViewFilter)
    TextView textViewFilter;

    @InjectView(R.id.textViewEdit)
    TextView textViewEdit;

    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    @InjectView(R.id.relativeLayoutContainer)
    RelativeLayout relativeLayoutContainer;

    @InjectView(R.id.linearlayoutFilterContent)
    LinearLayout linearlayoutFilterContent;

    @InjectView(R.id.listViewFiltermenu)
    ListView listViewFiltermenu;

    private BookingsPage pageBookings;
    private ProfilePage pageProfile;
    private SetAvailablePage pageSetAvailablePage;
    private SettingPage pageSetting;


    private BasePage currentPage;

    private ArrayList<BasePage> arrayListPages;
    private static MainActivity instance = null;

    public final int SHORT_GO_EDIT = 300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        App.getApp().addActivity(this);
        App.getApp().setMaincreate(true);
        App.getApp().setMainActivity(this);

        //=====fix nexus bug
        String model = android.os.Build.MODEL;
        String vsystem = android.os.Build.VERSION.RELEASE;
        View contentView = View.inflate(this, R.layout.activity_main, null);


        if (model.toLowerCase().startsWith("nexus")) {
            int nbarheight = Tool.getNavigationBarHeight(((Activity) getContext()));
            if (nbarheight > 0) {
                ((ViewGroup) contentView).setPadding(0, 0, 0, nbarheight);
            }
        }


//        if ("Nexus 5".equals(model) && ("5.1".equals(vsystem) || "6.0".equals(vsystem)) || "Nexus 6P".equals(model)) {
//            int nbarheight = Tool.getNavigationBarHeight(((Activity) getContext()));
//            if (nbarheight > 0) {
//                ((ViewGroup) contentView).setPadding(0, 0, 0, nbarheight);
//            }
//        }

        //=========
        setContentView(contentView);
        ButterKnife.inject(this);

        initView();

        showPage(pageBookings);


    }

    public void initView() {
        arrayListPages = new ArrayList<BasePage>();

        findViewById(R.id.viewMenu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggle();
            }
        });

        showEditMenu();
        RelativeLayout.LayoutParams rpp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        pageBookings = new BookingsPage(this);
        pageBookings.setTitle("Job list");
        relativeLayoutContainer.addView(pageBookings.getRootView(), rpp);
        arrayListPages.add(pageBookings);


        pageProfile = new ProfilePage(this);

        pageProfile.setTitle(getResources().getString(R.string.top_menu_profile));
        relativeLayoutContainer.addView(pageProfile.getRootView(), rpp);
        arrayListPages.add(pageProfile);


        pageSetAvailablePage = new SetAvailablePage(this);
        pageSetAvailablePage.setTitle("Availability");
        relativeLayoutContainer.addView(pageSetAvailablePage.getRootView(), rpp);
        arrayListPages.add(pageSetAvailablePage);


        pageSetting = new SettingPage(this);
        pageSetting.setTitle("Settings");
        relativeLayoutContainer.addView(pageSetting.getRootView(), rpp);
        arrayListPages.add(pageSetting);


        setOnSlideMenuClickListener(new SlideMenuFragment.OnSlideMenuClickListener() {
            @Override
            public void onLogOutClick() {

                toggle();

                ConfirmDialog confirmDialog = new ConfirmDialog(MainActivity.this, "Logout", "Do you confirm to logout?");

                confirmDialog.setOnConfirmDialogListener(new ConfirmDialog.OnConfirmDialogListener() {
                    @Override
                    public void OnConfirmClick() {

                        OneSignal.sendTag("userid", "-1");

                        App.getApp().setIslgout(true);

                        finish();
                    }

                    @Override
                    public void OncancelClick() {
                        toggle();
                    }
                });


                confirmDialog.show();
                //onClick logout
//                finish();
            }
        });

        initFilterMenu();
    }


    private void initFilterMenu() {

        String[] values = {"0", "1", "2", "3", "4"};
        String[] labels = {"View All Jobs", "View New Jobs", "View Accepted Jobs", "View Declined Jobs", "View Canceled Jobs"};
        final FilterAdapter filterAdapter = new FilterAdapter(this, values, labels);

        listViewFiltermenu.setAdapter(filterAdapter);

        listViewFiltermenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hideFilterMenuContent();
                filterAdapter.setIndex(position);
                pageBookings.onFilterTypeMenuClick(String.valueOf(position));
            }
        });

    }

    private void showFilterMenuContent() {

        linearlayoutFilterContent.clearAnimation();

        Animation animationShow = AnimationUtils.loadAnimation(this, R.anim.animation_show_filter);

        MyAnimationUtils.animationShowView(linearlayoutFilterContent, animationShow);


    }

    public void hideFilterMenuContent() {
        linearlayoutFilterContent.clearAnimation();
        Animation animationhide = AnimationUtils.loadAnimation(this, R.anim.animation_hide_filter);
        MyAnimationUtils.animationHideview(linearlayoutFilterContent, animationhide);
    }


    @OnClick(R.id.textViewEdit)
    public void onClickEdit() {
        Tool.startActivityForResult(MainActivity.this, EditProfileActivity.class, SHORT_GO_EDIT);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && !getSlidingMenu().isMenuShowing()) {
            exitBy2Click();
        }
        return false;
    }

    private static Boolean isExit = false;

    private void exitBy2Click() {
        Timer tExit = null;
        if (isExit == false) {
            isExit = true; // 准备退出
            Toast.makeText(this, "Click again to exit app!", Toast.LENGTH_SHORT).show();
            tExit = new Timer();
            tExit.schedule(new TimerTask() {
                @Override
                public void run() {
                    isExit = false; // 取消退出
                }
            }, 2000); //
        } else {
            App.getApp().exit();
        }
    }


    public void showBookingsView() {
        showPage(pageBookings);
    }


    public void showBookingsViewAndRefresh() {

        pageBookings.needRefresh = true;
        pageSetAvailablePage.needRefresh = true;

        showBookingsView();

    }


    public void showProfileView() {
        showPage(pageProfile);
    }

    public void showSettingView() {
        showPage(pageSetting);
    }

    public void showAvaiablepage() {
        showPage(pageSetAvailablePage);
    }


    private BasePage showPage(BasePage basePage) {

        for (BasePage page : arrayListPages) {

            if (page.equals(basePage)) {

                page.getRootView().setVisibility(View.VISIBLE);
                page.onShow();
                currentPage = page;
                textViewTitle.setText(page.getTitle());

                if (page instanceof ProfilePage) {
                    hideFilterMenu();
                    showEditMenu();
                } else if (page instanceof BookingsPage) {
                    hideEditMenu();
                    showFilterMenu();
                } else {
                    hideEditMenu();
                    hideFilterMenu();
                }
            } else {
                page.getRootView().setVisibility(View.INVISIBLE);
                page.onHide();
            }
        }

        return basePage;
    }

    public void hideEditMenu() {
        textViewEdit.setVisibility(View.GONE);
    }

    public void showEditMenu() {
        textViewEdit.setVisibility(View.VISIBLE);
    }

    public void hideFilterMenu() {
        textViewFilter.setVisibility(View.GONE);
        hideFilterMenuContent();
    }

    public void showFilterMenu() {
        textViewFilter.setVisibility(View.VISIBLE);

    }

    @OnClick(R.id.textViewFilter)
    public void onTextViewFilterClick() {

        if (linearlayoutFilterContent.getVisibility() == View.VISIBLE) {
            hideFilterMenuContent();
        } else {
            showFilterMenuContent();
        }


    }


    @Override
    protected void onResume() {
        super.onResume();

        if (null != currentPage) {
            currentPage.onResume();
        }

        App.getApp().activityResumed();

    }

    @Override
    protected void onPause() {
        super.onPause();

        App.getApp().activityPaused();
    }


    @Deprecated
    public void onPushReceive(JSONObject pushdata) {

        LogUtil.e(App.tag, "push info on CropMainActivity:" + pushdata.toString());

        String title = pushdata.optString("title", getResources().getString(R.string.app_name));
        String alert = pushdata.optString("alert", "Notification received.");
        final int type = pushdata.optInt("type", -1);


        NotifyDialog notifyDialog = new NotifyDialog(this, alert);
        notifyDialog.setOnOKclickListener(new NotifyDialog.OnOKclickListener() {
            @Override
            public void onOkClick() {


                if (type == 1) {//new job

                } else if (type == 2) {//cancel

                }
            }
        });

        notifyDialog.show();


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (null != currentPage) {
            currentPage.onActivityResult(requestCode, resultCode, data);
        }


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        App.getApp().setMaincreate(false);
        App.getApp().setMainActivity(this);
    }


    public void acceptDelayJob(final Context context, final boolean showDialog, final String jobId, final boolean acptDey, final NetCallBackMini netCallBack) {

        reSetTask();

        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.hideDialogSelf();
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {

                    JSONObject requestObj = new JSONObject();
                    requestObj.put("job_id", jobId);
                    requestObj.put("deal", acptDey ? "1" : "0");

                    netResult = NetInterface.acetDenyJob(context, requestObj);
                } catch (Exception e) {
                }


                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != netCallBack) {
                    netCallBack.onFinish(result);
                }

            }
        });
        baseTask.execute(new HashMap<String, String>());

    }
}
