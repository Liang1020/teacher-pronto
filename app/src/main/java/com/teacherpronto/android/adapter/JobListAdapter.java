package com.teacherpronto.android.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.view.TimerTextView;
import com.squareup.picasso.Picasso;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;
import com.teacherpronto.android.bean.Job;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 16/5/3.
 */
public class JobListAdapter extends BaseAdapter {

    private ArrayList<Job> arrayListJobs;
    private Context context;
    private LayoutInflater layoutInflater;
    private OnJobItemClickListener onJobItemClickListenerl;

    public JobListAdapter(Context context, ArrayList<Job> arrayListJobs) {

        this.context = context;
        this.arrayListJobs = arrayListJobs;
        this.layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return arrayListJobs.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListJobs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_job, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Job job = (Job) getItem(position);


        String summery = "<b>Job Summary</b>&nbsp;" + job.getJobTitle();


        viewHolder.textViewSummryTitle.setText(Html.fromHtml(summery));


        if (job.getType().equals(Job.STATUS_ACCEPT)) {
            viewHolder.linearLayoutBottomOperate.setVisibility(View.GONE);
            viewHolder.textViewTime.setVisibility(View.GONE);
            viewHolder.imageViewDotStatus.setImageResource(R.drawable.icon_dot_green);
            viewHolder.textViewStatusJob.setText("Accepted");
            viewHolder.textViewTime.setVisibility(View.INVISIBLE);
        }


        if (job.getType().equals(Job.STATUS_NEW)) {


            if (0 >= job.getTime_out_second()) {

                LogUtil.e(App.tag, "***job list error offter end time empty for new job");
                //this is error happpend

                viewHolder.linearLayoutBottomOperate.setVisibility(View.GONE);
                viewHolder.textViewTime.setVisibility(View.GONE);
                viewHolder.imageViewDotStatus.setImageResource(R.drawable.icon_dot_red);
                viewHolder.textViewStatusJob.setText("Declined");
                viewHolder.textViewTime.setVisibility(View.INVISIBLE);

            }else{

                viewHolder.linearLayoutBottomOperate.setVisibility(View.VISIBLE);
                viewHolder.textViewTime.setVisibility(View.VISIBLE);
                viewHolder.imageViewDotStatus.setImageResource(R.drawable.icon_dot_blue);

                viewHolder.textViewStatusJob.setText("NewJob");
                viewHolder.textViewTime.setVisibility(View.VISIBLE);

                viewHolder.textViewTime.setTimerOut((job.getTime_out_second()));
                viewHolder.textViewTime.setOnTimeOutListener(new TimerTextView.OnTimeOutListener() {
                    @Override
                    public void onTimeout(TimerTextView tx) {

//                    tx.setVisibility(View.INVISIBLE);
                        job.setType(Job.STATUS_DECLINE);
                        notifyDataSetChanged();
                    }
                });
            }
        }

        if (job.getType().equals(Job.STATUS_DECLINE)) {
            viewHolder.linearLayoutBottomOperate.setVisibility(View.GONE);
            viewHolder.textViewTime.setVisibility(View.GONE);
            viewHolder.imageViewDotStatus.setImageResource(R.drawable.icon_dot_red);
            viewHolder.textViewStatusJob.setText("Declined");
            viewHolder.textViewTime.setVisibility(View.INVISIBLE);
        }
        if (job.getType().equals(Job.STATUS_CANCELED)) {
            viewHolder.linearLayoutBottomOperate.setVisibility(View.GONE);
            viewHolder.textViewTime.setVisibility(View.GONE);
            viewHolder.imageViewDotStatus.setImageResource(R.drawable.icon_dot_canceled);
            viewHolder.textViewStatusJob.setText("Canceled");
            viewHolder.textViewTime.setVisibility(View.INVISIBLE);
        }


        if (null != job.getSchool() && (!StringUtils.isEmpty(job.getSchool().getLog()))) {
//            viewHolder.imageViewSchoolImg
            Picasso.with(App.getApp()).load(job.getSchool().getLog()).into(viewHolder.imageViewSchoolImg);
        }


        viewHolder.textViewSchoolName.setText(job.getSchool().getSchoolName());


        String durationTime = job.getStartDateText() + " - " + job.getEndDateText();


        viewHolder.textViewJobDuration.setText(durationTime);

        viewHolder.linearLayoutCenterContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onJobItemClickListenerl) {
                    onJobItemClickListenerl.onClickItem(job, position);
                }

            }
        });

        viewHolder.buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onJobItemClickListenerl) {
                    onJobItemClickListenerl.onAcceptClick(job, position);
                }
            }
        });

        viewHolder.buttonDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onJobItemClickListenerl) {
                    onJobItemClickListenerl.onDeclineClick(job, position);
                }
            }
        });


        return convertView;
    }


    public OnJobItemClickListener getOnJobItemClickListenerl() {
        return onJobItemClickListenerl;
    }

    public void setOnJobItemClickListenerl(OnJobItemClickListener onJobItemClickListenerl) {
        this.onJobItemClickListenerl = onJobItemClickListenerl;
    }


    static class ViewHolder {

        @InjectView(R.id.textViewSchoolName)
        TextView textViewSchoolName;

        @InjectView(R.id.textViewTime)
        TimerTextView textViewTime;

        @InjectView(R.id.textViewStatusJob)
        TextView textViewStatusJob;

        @InjectView(R.id.textViewJobDuration)
        TextView textViewJobDuration;

        @InjectView(R.id.imageViewSchoolImg)
        ImageView imageViewSchoolImg;

        @InjectView(R.id.textViewSummryTitle)
        TextView textViewSummryTitle;

        @InjectView(R.id.buttonAccept)
        Button buttonAccept;

        @InjectView(R.id.buttonDecline)
        Button buttonDecline;

        @InjectView(R.id.linearLayoutCenterContent)
        LinearLayout linearLayoutCenterContent;

        @InjectView(R.id.linearLayoutBottomOperate)
        LinearLayout linearLayoutBottomOperate;

        @InjectView(R.id.imageViewDotStatus)
        ImageView imageViewDotStatus;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }


    }


    public static interface OnJobItemClickListener {

        public void onClickItem(Job job, int index);

        public void onAcceptClick(Job job, int index);

        public void onDeclineClick(Job job, int index);
    }

}
