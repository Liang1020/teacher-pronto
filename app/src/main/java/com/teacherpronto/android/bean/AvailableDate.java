package com.teacherpronto.android.bean;

import android.content.Context;

import com.common.util.DateTool;
import com.common.util.SharePersistent;

import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by clia5292 on 2016/4/26.
 */
public class AvailableDate implements Serializable{
    private static final String KEY_STATE="state";

    private int year;
    private int month;
    private int day;

    private Context context;

    public AvailableDate(int year, int month, int day){
       this.year = year;
       this.month = month;
       this.day = day;
    }

    public AvailableDate(Context context){
        this.context = context;
        this.year = Calendar.getInstance().get(Calendar.YEAR);
        this.month = Calendar.getInstance().get(Calendar.MONTH) + 1;
        this.day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    }

    public static AvailableDate modifyDayForObject(AvailableDate date,int day){
        AvailableDate modifiDate = new AvailableDate(date.year,date.month,day);
        return modifiDate;
    }

    public void saveState(String key, int pos,int availability){
        SharePersistent.savePreference(context, KEY_STATE + "_" + key + "_" + pos, availability);
    }

    public int getState(String key,int pos){
        return SharePersistent.getInt(context, KEY_STATE + "_" + key + "_" + pos);
    }

    public void saveAvailability(String key, int pos,int availability){
        SharePersistent.savePreference(context, KEY_STATE + "_" + key + "_" + pos, availability);
    }

    public int getAvailability(String key,int pos){
        return SharePersistent.getInt(context, KEY_STATE + "_" + key + "_" + pos);
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
