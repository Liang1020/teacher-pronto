package com.teacherpronto.android.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Window;
import android.view.WindowManager;

import com.common.util.SharePersistent;
import com.common.util.Tool;
import com.indicator.view.IndicatorView;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;
import com.teacherpronto.android.adapter.GuildPagerAdapter;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class GuidActivity extends BaseTeacherActivity {

    @InjectView(R.id.viewPager)
    ViewPager viewPager;

    @InjectView(R.id.indicatorView)
    IndicatorView indicatorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_guid);
        ButterKnife.inject(this);
        App.getApp().addActivity(this);

        GuildPagerAdapter guildPagerAdapter = new GuildPagerAdapter(this);

        guildPagerAdapter.setOnSkipOrStartClicKListener(new GuildPagerAdapter.OnSkipOrStartClicKListener() {
            @Override
            public void onClickSkip() {
                SharePersistent.saveBoolean(GuidActivity.this, "see_guild", true);
                Tool.startActivity(GuidActivity.this, LoginActivity.class);
                finish();
            }
        });


        viewPager.setAdapter(guildPagerAdapter);
        indicatorView.setIndicatorSize(5, R.drawable.icon_dot_uncheck, R.drawable.icon_dot_checnk);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                indicatorView.checkIndex(position);
            }
        });

    }
}
