package com.teacherpronto.android.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.bigkoo.pickerview.view.BasePickerView;
import com.common.util.StringUtils;
import com.teacherpronto.android.R;

public class PhotoSelectDialog extends BasePickerView {

    public OnPhotoSelectionActionListener onPhotoSelectionActionListener;

    private TextView textViewTitle;

    public PhotoSelectDialog(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.dialog_photo_select, contentContainer);

        textViewTitle = (TextView) findViewById(R.id.textViewTitle);

        findViewById(R.id.buttonDismiss).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onPhotoSelectionActionListener) {
                    onPhotoSelectionActionListener.onDismis();
                }
            }
        });

        findViewById(R.id.buttonChosePhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onPhotoSelectionActionListener) {
                    onPhotoSelectionActionListener.onChoosePhotoClick();
                }
            }
        });

        findViewById(R.id.buttonTakePhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();

                if (null != onPhotoSelectionActionListener) {
                    onPhotoSelectionActionListener.onTakePhotoClick();
                }
            }
        });
    }

    public void setTextViewTitle(String title) {

        if (StringUtils.isEmpty(title)) {
            textViewTitle.setVisibility(View.GONE);
        } else {
            textViewTitle.setVisibility(View.VISIBLE);
        }
        textViewTitle.setText(title);

    }


    public static interface OnPhotoSelectionActionListener {

        public void onTakePhotoClick();

        public void onChoosePhotoClick();

        public void onDismis();
    }

    public OnPhotoSelectionActionListener onPhotoSelectionAction() {
        return onPhotoSelectionActionListener;
    }

    public void setOnAddCatchListener(OnPhotoSelectionActionListener onAddCatchListener) {
        this.onPhotoSelectionActionListener = onAddCatchListener;
    }
}