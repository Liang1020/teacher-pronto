package com.teacherpronto.android.json;

import com.common.net.NetResult;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.teacherpronto.android.App;
import com.teacherpronto.android.bean.Availability;
import com.teacherpronto.android.bean.Job;
import com.teacherpronto.android.bean.School;
import com.teacherpronto.android.bean.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by clia on 2016/5/23.
 */
public class JSONHelper {

    private static final String TAG = App.tag;

    public static final String STATUS_OK = NetResult.CODE_OK;
    public static final String STATUS_ERROR = NetResult.CODE_ERROR;

    public static final String status_tag = "status";
    public static final String msg_tag = "message";
    public static final String content_tag = "content";
    public static final String token_tag = "api_token";
    public static final String tag_token = "token";
    public static final String error_tag = "error";

    public static NetResult parseRegister(InputStream inputStream) throws JSONException {

        JSONObject jsonObject = new JSONObject(getJsonFromInputStream(inputStream));
        LogUtil.i(TAG, jsonObject.toString());

        NetResult netResult = new NetResult();


        netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
        netResult.setCode(getStringFroJso(status_tag, jsonObject));
        netResult.setErrors(getStringFroJso(error_tag, jsonObject));

        if (netResult.isOk()) {

            JSONObject dataObj = jsonObject.getJSONObject(content_tag);
            User user = parseUserFromJsonObject(dataObj);
            String token = user.getToken();
            netResult.setData(new Object[]{user});
            App.getApp().setToken(token);

        }

        return netResult;

    }

    public static NetResult parseLogin(InputStream inputStream) throws Exception {
        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(App.tag, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();

        netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
        netResult.setCode(getStringFroJso(status_tag, jsonObject));
        netResult.setErrors(getStringFroJso(error_tag, jsonObject));

        if (netResult.isOk()) {


            JSONObject userObj = jsonObject.getJSONObject(content_tag);

            User user = parseUserFromJsonObject(userObj);

            String token = user.getToken();

            netResult.setData(new Object[]{user, token});

//            App.getApp().setUser(user);

            App.getApp().setToken(token);

            App.getApp().setVerified(Integer.parseInt(user.getVerify()));
        }

        return netResult;
    }


    private static User parseUserFromJsonObject(JSONObject userObj) throws JSONException {

        User user = new User();


        user.setUser_id(getStringFroJso("user_id", userObj));
        user.setEmail(getStringFroJso("email", userObj));
        user.setFirst_name(getStringFroJso("first_name", userObj));
        user.setLast_name(getStringFroJso("last_name", userObj));
        user.setRegister_number(getStringFroJso("register_number", userObj));
        user.setPost_code(getStringFroJso("postcode", userObj));
        user.setLatitude(getStringFroJso("lat", userObj));
        user.setLongitude(getStringFroJso("lng", userObj));
        user.setAvatar(getStringFroJso("avatar", userObj));

        user.setPush_notification(getStringFroJso("push_notification", userObj));
        user.setEmail_notification(getStringFroJso("email_notification", userObj));
        user.setDistance_preference(getStringFroJso("distance_preference", userObj));

        user.setForm_lodged(getStringFroJso("form_lodged", userObj));
        user.setRegister_type(getStringFroJso("register_type", userObj));
        user.setRegister_state(getStringFroJso("register_state", userObj));


        user.setPhone(getStringFroJso("phone", userObj));
        user.setTeaching_preference(getStringFroJso("teaching_preference", userObj));


        if (userObj.has("subjects_preference") && !StringUtils.isEmpty(userObj.getString("subjects_preference")) && !"null".equals(userObj.getString("subjects_preference"))) {

            JSONArray teachSubjectsArray = userObj.getJSONArray("subjects_preference");


            if (null != teachSubjectsArray && teachSubjectsArray.length() > 0) {

                for (int i = 0, isize = teachSubjectsArray.length(); i < isize; i++) {
                    user.addSubjectsPreference(teachSubjectsArray.getString(i));
                }
            }

        }


        if (userObj.has("level_experience") && !StringUtils.isEmpty(userObj.getString("level_experience")) && !"null".equals(userObj.getString("level_experience"))) {
            JSONArray levelexpercesArray = userObj.getJSONArray("level_experience");

            if (null != levelexpercesArray && levelexpercesArray.length() > 0) {
                for (int i = 0, isize = levelexpercesArray.length(); i < isize; i++) {
                    user.addLevelExperences(levelexpercesArray.getString(i));
                }
            }
        }


        user.setYear_experience(getStringFroJso("year_experience", userObj));
        user.setIndroduction(getStringFroJso("introduction", userObj));
        user.setVerify(getStringFroJso("verified", userObj));


        String token = getStringFroJso("api_token", userObj);

        user.setToken(token);


        return user;

    }


    public static NetResult parseMe(InputStream inputStream) throws Exception {
        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();

        netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
        netResult.setCode(getStringFroJso(status_tag, jsonObject));
        netResult.setErrors(getStringFroJso(error_tag, jsonObject));


        if (netResult.isOk()) {

            User user = new User();

            JSONObject userObj = jsonObject.getJSONObject(content_tag);

            user.setUser_id(getStringFroJso("user_id", userObj));
            user.setEmail(getStringFroJso("email", userObj));
            user.setFirst_name(getStringFroJso("first_name", userObj));
            user.setLast_name(getStringFroJso("last_name", userObj));
            user.setRegister_number(getStringFroJso("regist_number", userObj));
            user.setPost_code(getStringFroJso("post_code", userObj));
            user.setLatitude(getStringFroJso("latitude", userObj));
            user.setLongitude(getStringFroJso("longitude", userObj));
            user.setAvatar(getStringFroJso("avatar", userObj));
            user.setPush_notification(getStringFroJso("push_notification", userObj));
            user.setEmail_notification(getStringFroJso("email_notification", userObj));
            user.setDistance_preference(getStringFroJso("distance_preference", userObj));

            netResult.setData(new Object[]{user});

            App.getApp().setUser((User) netResult.getData()[0]);
        }

        return netResult;
    }

    public static NetResult parseVerification(InputStream inputStream) throws JSONException {
        String result = getJsonFromInputStream(inputStream);


        LogUtil.e(App.tag, "verify:" + result);

        LogUtil.i(TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();

        netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
        netResult.setCode(getStringFroJso(status_tag, jsonObject));
        netResult.setErrors(getStringFroJso(error_tag, jsonObject));

        return netResult;
    }

    public static NetResult parseAvailability(InputStream inputStream) throws JSONException {
        String result = getJsonFromInputStream(inputStream);
        LogUtil.i(TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();

        netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
        netResult.setCode(getStringFroJso(status_tag, jsonObject));
        netResult.setErrors(getStringFroJso(error_tag, jsonObject));

        if (netResult.isOk()) {

            JSONArray array = jsonObject.getJSONArray(content_tag);

            String format = "";
            String year, month, day;
            ArrayList<Availability> avail = new ArrayList<>();

            for (int i = 0; i < array.length(); i++) {
                Availability availability = new Availability();

                availability.setId(getStringFroJso("id", array.getJSONObject(i)));
                availability.setUser_id(getStringFroJso("user_id", array.getJSONObject(i)));

                format = getStringFroJso("date", array.getJSONObject(i));
                year = format.substring(0, 4);
                month = format.substring(format.indexOf("-") + 1, format.lastIndexOf("-"));
                day = format.substring(format.lastIndexOf("-") + 1);

                if (month.substring(0, 1).equals("0")) {
                    month = month.substring(1);
                }

                if (day.substring(0, 1).equals("0")) {
                    day = day.substring(1);
                }

                availability.setYear(year);
                availability.setMonth(month);
                availability.setDay(day);

                availability.setAvailability(getStringFroJso("availability", array.getJSONObject(i)));
                availability.setBooking(getStringFroJso("booking", array.getJSONObject(i)));
                avail.add(availability);

            }
            netResult.setData(new Object[]{avail});
        }

        return netResult;
    }

    public static NetResult parseAvailabilityResponse(InputStream inputStream) throws JSONException {
        String result = getJsonFromInputStream(inputStream);
        LogUtil.i(TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();

        netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
        netResult.setCode(getStringFroJso(status_tag, jsonObject));
        netResult.setErrors(getStringFroJso(error_tag, jsonObject));

        if (netResult.isOk()) {

            JSONObject obj = jsonObject.getJSONObject(content_tag);

            JSONArray array = obj.getJSONArray("dates");

            LogUtil.i("AvailResponse", getStringFroJso("user_id", obj) + " " + getStringFroJso("date", obj)
                    + " " + getStringFroJso("availability", obj) + " " + array.toString() + " " + getStringFroJso("id", obj));

        }

        return netResult;
    }

    public static NetResult parseProfile(InputStream inputStream) throws Exception {

        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();

        netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
        netResult.setCode(getStringFroJso(status_tag, jsonObject));
        netResult.setErrors(getStringFroJso(error_tag, jsonObject));

        if (netResult.isOk()) {

//            User user = new User();
//
            JSONObject userObj = jsonObject.getJSONObject(content_tag);
//
//            user.setUser_id(getStringFroJso("user_id", userObj));
//            user.setEmail(getStringFroJso("email", userObj));
//            user.setFirst_name(getStringFroJso("first_name", userObj));
//            user.setLast_name(getStringFroJso("last_name", userObj));
//            user.setRegister_number(getStringFroJso("regist_number", userObj));
//            user.setPost_code(getStringFroJso("post_code", userObj));
//            user.setLatitude(getStringFroJso("lat", userObj));
//            user.setLongitude(getStringFroJso("lng", userObj));
//            user.setAvatar(getStringFroJso("avatar", userObj));
//
//            user.setPush_notification(getStringFroJso("push_notification", userObj));
//            user.setEmail_notification(getStringFroJso("email_notification", userObj));
//            user.setDistance_preference(getStringFroJso("distance_preference", userObj));
//
//            user.setForm_lodged(getStringFroJso("form_lodged", userObj));
//            user.setRegister_type(getStringFroJso("register_type", userObj));
//            user.setRegister_state(getStringFroJso("register_state", userObj));
//
//
//            user.setPhone(getStringFroJso("phone", userObj));
//            user.setTeaching_preference(getStringFroJso("teaching_preference", userObj));
//
//
//            JSONArray teachSubjectsArray = userObj.getJSONArray("subjects_preference");
//
//
//            if (null != teachSubjectsArray && teachSubjectsArray.length() > 0) {
//
//                for (int i = 0, isize = teachSubjectsArray.length(); i < isize; i++) {
//                    user.addSubjectsPreference(teachSubjectsArray.getString(i));
//                }
//            }
//
//
//            JSONArray levelexpercesArray = userObj.getJSONArray("level_experience");
//
//            if (null != levelexpercesArray && levelexpercesArray.length() > 0) {
//                for (int i = 0, isize = levelexpercesArray.length(); i < isize; i++) {
//                    user.addLevelExperences(levelexpercesArray.getString(i));
//                }
//            }
//
//            user.setYear_experience(getStringFroJso("year_experience", userObj));
//            user.setIndroduction(getStringFroJso("introduction", userObj));
//            user.setVerify(getStringFroJso("verified", userObj));

            netResult.setData(new Object[]{parseUserFromJsonObject(userObj)});

            App.getApp().setUser((User) netResult.getData()[0]);
        }

        return netResult;

    }

    public static NetResult parseUpdateSetting(InputStream inputStream) throws JSONException {
        String result = getJsonFromInputStream(inputStream);

        LogUtil.i(TAG, result);

        JSONObject jsonObject = new JSONObject(result);
        NetResult netResult = new NetResult();

        netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
        netResult.setCode(getStringFroJso(status_tag, jsonObject));
        netResult.setErrors(getStringFroJso(error_tag, jsonObject));

        return netResult;
    }


    public static NetResult parseUpload(String json) throws JSONException {

        LogUtil.i(TAG, "result upload json:" + json);

        JSONObject jsonObject = new JSONObject(json);
        NetResult netResult = new NetResult();

        netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
        netResult.setCode(getStringFroJso(status_tag, jsonObject));
        netResult.setErrors(getStringFroJso(error_tag, jsonObject));

        if (netResult.isOk()) {

            JSONObject dataObj = jsonObject.getJSONObject(content_tag);

            netResult.setData(new Object[]{dataObj.getString("avatar")});

        }

        return netResult;
    }


    public static NetResult parseCommonSimple(InputStream inputStream) throws JSONException {

        JSONObject jsonObject = new JSONObject(getJsonFromInputStream(inputStream));
        LogUtil.i(TAG, jsonObject.toString());

        NetResult netResult = new NetResult();

        netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
        netResult.setCode(getStringFroJso(status_tag, jsonObject));
        netResult.setErrors(getStringFroJso(error_tag, jsonObject));


        return netResult;

    }


    public static NetResult parseJobList(InputStream inputStream) throws JSONException {

        JSONObject jsonObject = new JSONObject(getJsonFromInputStream(inputStream));
        LogUtil.i(TAG, jsonObject.toString());

        NetResult netResult = new NetResult();

        netResult.setMessage(getStringFroJso(msg_tag, jsonObject));
        netResult.setCode(getStringFroJso(status_tag, jsonObject));
        netResult.setErrors(getStringFroJso(error_tag, jsonObject));

        ArrayList<Job> jobsList = new ArrayList<>();
        int totals = 0;

        if (netResult.isOk()) {

            try {

                totals = jsonObject.getJSONObject(content_tag).getInt("total");


                JSONArray jobs = jsonObject.getJSONObject(content_tag).getJSONArray("jobs");
                if (null != jobs && jobs.length() > 0) {

                    for (int i = 0, isize = jobs.length(); i < isize; i++) {

                        JSONObject jobObject = jobs.getJSONObject(i);

                        Job job = new Job();

                        job.setJobId(getStringFroJso("job_id", jobObject));
                        job.setType(getStringFroJso("type", jobObject));
                        job.setTypeDescription(getStringFroJso("type_description", jobObject));
                        job.setStartDate(getStringFroJso("start_date", jobObject));

                        job.setEndDate(getStringFroJso("end_date", jobObject));
                        job.setStartTime(getStringFroJso("start_time", jobObject));
                        job.setEndTime(getStringFroJso("end_time", jobObject));
                        job.setTeacherType(getStringFroJso("teacher_type", jobObject));


                        JSONArray teachingLevels = jobObject.getJSONArray("teaching_level");
                        if (null != teachingLevels && teachingLevels.length() > 0) {
                            for (int x = 0, xisize = teachingLevels.length(); x < xisize; x++) {
                                job.addTeahingLeve(teachingLevels.getString(x));
                            }
                        }


                        job.setJobTitle(getStringFroJso("job_title", jobObject));
                        job.setJobDescription(getStringFroJso("job_description", jobObject));

                        try {
                            job.setTime_out_second(jobObject.getInt("time_out_second"));
                        } catch (Exception e) {
                            job.setTime_out_second(-1);
                        }

                        try {

                            String offendEndTime = getStringFroJso("offer_end_time", jobObject);  //offer_end_time

                            if (StringUtils.isEmpty(offendEndTime)) {
                                job.setOffer_end_time(-1);
                            } else {

                                Long offerendTime = Long.parseLong(offendEndTime);
                                job.setOffer_end_time(offerendTime);

                                LogUtil.e(App.tag, "time end:" + (System.currentTimeMillis() / 1000 - offerendTime));
                            }

                        } catch (Exception e) {
                            LogUtil.e(App.tag, "parse offer end time error:" + e.getMessage());
                            job.setOffer_end_time(-1);
                        }


                        job.setCanced(getStringFroJso("cancelled", jobObject));
                        job.setExpired(getStringFroJso("expired", jobObject));


                        JSONObject schoolObj = jobObject.getJSONObject("school");

                        School school = new School();

                        school.setSchoolName(getStringFroJso("school_name", schoolObj));
                        school.setSchoolType(getStringFroJso("school_type", schoolObj));
                        school.setContactName(getStringFroJso("contact_name", schoolObj));
                        school.setContactEmail(getStringFroJso("contact_email", schoolObj));
                        school.setContactPhone(getStringFroJso("contact_phone", schoolObj));

                        school.setAddressTreet(getStringFroJso("address_street", schoolObj));
                        school.setAddressSuburb(getStringFroJso("address_suburb", schoolObj));
                        school.setAddressState(getStringFroJso("address_state", schoolObj));
                        school.setAddressPostCode(getStringFroJso("address_postcode", schoolObj));

                        school.setCurrentEnrolments(getStringFroJso("current_enrolments", schoolObj));
                        school.setLat(getStringFroJso("lat", schoolObj));
                        school.setLng(getStringFroJso("lng", schoolObj));
                        school.setLog(getStringFroJso("logo", schoolObj));

                        job.setSchool(school);

                        jobsList.add(job);

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                LogUtil.i(App.tag, "parse job list error:" + e.getLocalizedMessage());
            }

        }

        netResult.setData(new Object[]{jobsList, totals});

        return netResult;

    }


    //=============================================

    public static String getJsonFromInputStream(InputStream ins) {
        if (null != ins) {
            StringBuffer sbf = new StringBuffer();
            BufferedInputStream bfris = new BufferedInputStream(ins);
            byte[] buffer = new byte[512];
            int ret = -1;
            try {
                while ((ret = bfris.read(buffer)) != -1) {
                    sbf.append(new String(buffer, 0, ret));
                }
                bfris.close();
                ins.close();
                return sbf.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String getStringFroJso(String key, JSONObject jobj) throws JSONException {
        if (jobj.has(key)) {

            String result = jobj.getString(key);

            if ("null".equals(result)) {
                return "";
            } else {
                return result;
            }

        } else {
            return "";
        }
    }
}
