package com.teacherpronto.android.bean;

/**
 * Created by wangzy on 16/5/3.
 */
public class School {


    private String schoolName;
    private String schoolType;
    private String contactName;
    private String contactEmail;
    private String contactPhone;
    private String addressTreet;
    private String addressSuburb;
    private String addressState;
    private String addressPostCode;
    private String currentEnrolments;

    private String lat;
    private String lng;
    private String log;


    public School(){

    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolType() {
        return schoolType;
    }

    public void setSchoolType(String schoolType) {
        this.schoolType = schoolType;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getAddressTreet() {
        return addressTreet;
    }

    public void setAddressTreet(String addressTreet) {
        this.addressTreet = addressTreet;
    }

    public String getAddressSuburb() {
        return addressSuburb;
    }

    public void setAddressSuburb(String addressSuburb) {
        this.addressSuburb = addressSuburb;
    }

    public String getAddressState() {
        return addressState;
    }

    public void setAddressState(String addressState) {
        this.addressState = addressState;
    }

    public String getAddressPostCode() {
        return addressPostCode;
    }

    public void setAddressPostCode(String addressPostCode) {
        this.addressPostCode = addressPostCode;
    }

    public String getCurrentEnrolments() {
        return currentEnrolments;
    }

    public void setCurrentEnrolments(String currentEnrolments) {
        this.currentEnrolments = currentEnrolments;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}

