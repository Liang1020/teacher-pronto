package com.teacherpronto.android.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.util.StringUtils;
import com.teacherpronto.android.R;

/**
 * Created by wangzy on 16/5/5.
 */
public class ForgetPasswordDialog extends MyBasePicker {

    private OnConfirmDialogListener onConfirmDialogListener;

    private EditText editTextForgetPwd;

    public ForgetPasswordDialog(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_forgetpwd, contentContainer);

        initSubviews("", "");
    }

    public ForgetPasswordDialog(Context context, String title, String msg) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_accept_confirm, contentContainer);

        initSubviews(title, msg);

    }


    private void initSubviews(String title, String msg) {


        editTextForgetPwd = (EditText) findViewById(R.id.editTextForgetPwd);


        if (!StringUtils.isEmpty(title)) {
            ((TextView) findViewById(R.id.textViewTitle)).setText(title);
        }

        if (!StringUtils.isEmpty(msg)) {
            ((TextView) findViewById(R.id.textViewMsg)).setText(msg);
        }


        findViewById(R.id.buttonYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!StringUtils.isEmpty(editTextForgetPwd.getText().toString())) {
                    dismiss();
                    if (null != onConfirmDialogListener) {
                        onConfirmDialogListener.OnConfirmClick(editTextForgetPwd.getText().toString().trim());
                    }
                }else{

                    editTextForgetPwd.startAnimation(AnimationUtils.loadAnimation(context,R.anim.shake));
                }


            }
        });

        findViewById(R.id.buttonCancel).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onConfirmDialogListener) {

                    onConfirmDialogListener.OncancelClick();
                }
            }
        });

    }


    public OnConfirmDialogListener getOnConfirmDialogListener() {
        return onConfirmDialogListener;
    }

    public void setOnConfirmDialogListener(OnConfirmDialogListener onConfirmDialogListener) {
        this.onConfirmDialogListener = onConfirmDialogListener;
    }

    public static interface OnConfirmDialogListener {

        public void OnConfirmClick(String email);

        public void OncancelClick();
    }
}
