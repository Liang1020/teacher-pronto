package com.teacherpronto.android.net;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.common.net.FormFile;
import com.common.net.HttpRequester;
import com.common.net.NetResult;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.teacherpronto.android.App;
import com.teacherpronto.android.activity.LoginActivity;
import com.teacherpronto.android.json.JSONHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by clia on 2016/5/23.
 */
public class NetInterface {

//  private static String base_path = "http://106.185.55.185:8000/api/"; // testing server
//    private static String base_path = "http://103.18.108.130:8000/api/"; // product server

    private static String base_path = "https://portal.teacherpronto.com.au/api/"; // product server

    public static NetResult register(Context context, JSONObject customizedObject) throws Exception {
        String path = "auth/register";
        InputStream jsonInputStream = createCallBackInputStream(context, customizedObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseRegister(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult login(Context context, JSONObject jsonObject) throws Exception {
        String path = "auth/login";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseLogin(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getProfile(Context context, JSONObject jsonObject) throws Exception {
        String path = "me";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseProfile(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getVerify(Context context, JSONObject jsonObject) throws Exception {
        String path = "get_verify";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseVerification(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult verify(Context context, JSONObject jsonObject) throws Exception {
        String path = "verify";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseVerification(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult uploadAvatar(FormFile[] formFiles, HashMap<String, String> map) throws Exception {
        String path = "upload_avatar";
        String json = HttpRequester.postUpload(base_path + path, map, formFiles, App.getApp().getToken());
        if (null != json) {
            NetResult netResult = JSONHelper.parseUpload(json);
            return netResult;
        }
        return null;
    }

    public static NetResult uploadProfile(Context context, JSONObject jsonObject) throws Exception {
        String path = "profile_update";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseProfile(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getAvailability(Context context, JSONObject jsonObject) throws Exception {
        String path = "get_available_dates";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseAvailability(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult setAvailability(Context context, JSONObject jsonObject) throws Exception {
        String path = "set_available_date";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseAvailabilityResponse(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult updateSetting(Context context, JSONObject jsonObject) throws Exception {
        String path = "setting_update";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseUpdateSetting(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult getJobList(Context context, JSONObject jsonObject) throws Exception {
        String path = "job_list";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseJobList(jsonInputStream);
            return netResult;
        }
        return null;
    }


    public static NetResult acetDenyJob(Context context, JSONObject jsonObject) throws Exception {
        String path = "deal_job";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseCommonSimple(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult chagnePasswd(Context context, JSONObject jsonObject) throws Exception {
        String path = "change_password";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseCommonSimple(jsonInputStream);
            return netResult;
        }
        return null;
    }

    public static NetResult forgetPwd(Context context, JSONObject jsonObject) throws Exception {
        String path = "auth/reset_password";
        InputStream jsonInputStream = createCallBackInputStream(context, jsonObject, base_path + path, HttpRequester.REQUEST_POST);
        if (null != jsonInputStream) {
            NetResult netResult = JSONHelper.parseCommonSimple(jsonInputStream);
            return netResult;
        }
        return null;
    }



//    public static NetResult dealErrorCode(final Context context, final NetResult netResult) {
//    validation error
//        if ("422".equals(netResult.getCode()) && !(context instanceof LoginActivity)) {
//
//            Tool.showMessageDialog(netResult.getMessage(), (Activity) context, new DialogCallBackListener() {
//                @Override
//                public void onDone(boolean yesOrNo) {
//
//                    logout2Login(context, netResult);
//
//                }
//            });
//
//        } else if ("401".equals(netResult.getCode()) && !(context instanceof LoginActivity)) {
    //login error
//
//            Tool.showMessageDialog("Please relogin", (Activity) context, new DialogCallBackListener() {
//                @Override
//                public void onDone(boolean yesOrNo) {
//                    logout2Login(context, netResult);
//                }
//            });
//        }
//
//
//        return netResult;
//    }


    private static void logout2Login(Context context, NetResult netResult) {
        netResult.setErrors("");
        netResult.setMessage("");

        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        ((Activity) context).finish();
    }


    //=====================common method======================


    private static InputStream createCallBackInputStream(Context context, JSONObject customizedObject, String path, String requestType) throws Exception {


        JSONObject commonObject = new JSONObject();

        commonObject.put("app_version", Tool.getVersionName(context));
        commonObject.put("client_type", "0");
        commonObject.put("deviceId", Tool.getImei(context));

        customizedObject.put("common", commonObject);


        String jsonRequest = customizedObject.toString();


        //=========================

        String token = null;
        if (null != App.getApp().getUser() && !StringUtils.isEmpty(App.getApp().getToken())) {
            token = App.getApp().getToken();
        }


        LogUtil.i(App.tag, "path:" + path);
        LogUtil.i(App.tag, "" + jsonRequest);
        LogUtil.i(App.tag, "======token=======");
        LogUtil.e(App.tag, "" + token);
        LogUtil.i(App.tag, "======token end=======");
        InputStream jsonInputStream = HttpRequester.doHttpRequestText(context, path, jsonRequest, token, requestType);
        return jsonInputStream;
    }

    /**
     * create request Object from map to jsonobject
     *
     * @param paramMap
     * @return
     * @throws JSONException
     */
    public static JSONObject buildCustomizedObject(HashMap<String, String> paramMap) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        for (Map.Entry<String, String> entry : paramMap.entrySet()) {
            jsonObject.put(entry.getKey(), entry.getValue());
        }

        return jsonObject;
    }
}
