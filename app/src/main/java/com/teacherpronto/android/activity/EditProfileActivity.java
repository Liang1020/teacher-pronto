package com.teacherpronto.android.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.callback.OnUploadCallBack;
import com.common.adapter.AnimationListenerAdapter;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.view.RoundImageView;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;
import com.teacherpronto.android.adapter.SpninnerAdapter;
import com.teacherpronto.android.bean.User;
import com.teacherpronto.android.net.NetInterface;
import com.teacherpronto.android.util.PhotoLibUtils;
import com.teacherpronto.android.util.PhotoSelectDialog;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * Created by wangzy on 16/4/13.
 */
public class EditProfileActivity extends BaseTeacherActivity {

    @InjectView(R.id.roundImgView)
    RoundImageView roundImageView;

    @InjectView(R.id.spninnerLodEmployer)
    Spinner spninnerLodEmployer;

    @InjectView(R.id.spinnerRegisterd)
    Spinner spinnerRegisterd;

    @InjectView(R.id.spinnerRegisterdState)
    Spinner spinnerRegisterdState;

    @InjectView(R.id.spinnerTeachyear)
    Spinner spinnerTeachyear;


    @InjectView(R.id.textViewNotifyLabel)
    TextView textViewNotifyLabel;

    @InjectView(R.id.editTextFirstName)
    EditText editTextFirstName;

    @InjectView(R.id.editTextLastName)
    EditText editTextLastName;

    @InjectView(R.id.editTextTeacherRegistNumber)
    EditText editTextTeacherRegistNumber;

    @InjectView(R.id.editTextPhoneNumber)
    EditText editTextPhoneNumber;

    @InjectView(R.id.radioGroupTeachingPreference)
    RadioGroup radioGroupTeacherPreference;

    @InjectView(R.id.textViewAddSubject)
    TextView textViewAddSubject;

    @InjectView(R.id.linearLayoutTaughts)
    LinearLayout linearLayoutTaughts;


    @InjectView(R.id.checkBoxIsGraduate)
    CheckBox checkBoxIsGraduate;

    @InjectView(R.id.linearLayoutTeacherPre)
    LinearLayout linearLayoutTeacherPre;

    @InjectView(R.id.editTextSeflDesc)
    EditText editTextSelfDesc;


    @InjectView(R.id.checkBoxSelectAllYearLevel)
    CheckBox checkBoxSelectAllYearLevel;

    @InjectView(R.id.editTextZip)
    EditText editTextZip;

    @InjectView(R.id.viewBack)
    View viewBack;

    @InjectView(R.id.textViewDone)
    TextView textViewDone;


    LayoutInflater layoutInflater;


    @InjectView(R.id.viewClickDone)
    View viewClickDone;


    private ArrayList<EditText> arrayListEditTextTeacherPres;

    private ArrayList<CheckBox> arrayListCheckBoxTeachPrsPrimary;
    private ArrayList<CheckBox> arrayListCheckBoxSecondArys;
    private boolean isFromRegist = false;
    private Animation animationShake;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        ButterKnife.inject(this);
        App.getApp().addActivity(this);


        animationShake = AnimationUtils.loadAnimation(this, R.anim.shake);
        arrayListEditTextTeacherPres = new ArrayList<>();
        arrayListCheckBoxTeachPrsPrimary = new ArrayList<>();
        arrayListCheckBoxSecondArys = new ArrayList<>();

        layoutInflater = LayoutInflater.from(this);
        initView();

        if (App.getApp().containValidateBooleanKey("isFromRegist")) {

            viewBack.setVisibility(View.INVISIBLE);

            viewClickDone.setVisibility(View.VISIBLE);
            textViewDone.setVisibility(View.INVISIBLE);
            isFromRegist = true;
        } else {
            viewBack.setVisibility(View.VISIBLE);
            isFromRegist = false;
            viewClickDone.setVisibility(View.GONE);
            textViewDone.setVisibility(View.VISIBLE);
        }


    }


    @OnCheckedChanged(R.id.checkBoxIsGraduate)
    public void checkGraduate() {

        int visibile = checkBoxIsGraduate.isChecked() ? View.GONE : View.VISIBLE;

        linearLayoutTeacherPre.setVisibility(visibile);
    }


    @OnClick(R.id.viewAddObject)
    public void onClickAddTeachPre() {


        for (int i = 0, isize = arrayListEditTextTeacherPres.size(); i < isize; i++) {

            final EditText editTextResaved = arrayListEditTextTeacherPres.get(i);
            if (StringUtils.isEmpty(getInput(editTextResaved))) {

                animationShake.setDuration(1500);
                editTextResaved.startAnimation(animationShake);

                animationShake.setAnimationListener(new AnimationListenerAdapter() {

                    @Override
                    public void onAnimationStart(Animation animation) {
                        editTextResaved.setHint("Please input subject first.");
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        editTextResaved.setHint("");
                    }
                });

//                Tool.showMessageDialog("Please input subject first", EditProfileActivity.this);
                return;
            }

        }


        final View teacherPre = layoutInflater.inflate(R.layout.item_teach_pre, null);

        linearLayoutTaughts.addView(teacherPre);

        final EditText editTextPre = (EditText) teacherPre.findViewWithTag("editTextTeachPre");


        arrayListEditTextTeacherPres.add(editTextPre);

        View buttonDel = teacherPre.findViewWithTag("buttonDeleteTeachPre");

        buttonDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                linearLayoutTaughts.removeView(teacherPre);
                arrayListEditTextTeacherPres.remove(editTextPre);

                LogUtil.i(App.tag, "click del");
            }
        });


    }


    private void updateInfo() {


        LogUtil.e(App.tag, "=========================");


        String firstName = getInput(editTextFirstName);

        LogUtil.i(App.tag, "fistname:" + firstName);

        String lastName = getInput(editTextLastName);
        LogUtil.i(App.tag, "lastname:" + firstName);

        String employer = (String) spninnerLodEmployer.getSelectedItem();

        LogUtil.i(App.tag, "employer:" + employer);

        String registerdLocation = (String) spinnerRegisterd.getSelectedItem();
        LogUtil.i(App.tag, "registerdLocation:" + registerdLocation);


        String teacherNumber = getInput(editTextTeacherRegistNumber);
        LogUtil.i(App.tag, "teacherNumber:" + teacherNumber);

        String registedState = (String) spinnerRegisterdState.getSelectedItem();

        LogUtil.i(App.tag, "registedState:" + registedState);

        String phoneNumber = getInput(editTextPhoneNumber);
        LogUtil.i(App.tag, "phoneNumber:" + phoneNumber);

        int checkId = radioGroupTeacherPreference.getCheckedRadioButtonId();
        String teachPre = "";
        if (-1 != checkId) {
            teachPre = (String) findViewById(checkId).getTag();
            LogUtil.i(App.tag, "teachPre:" + teachPre);
        }

        for (EditText objectTeach : arrayListEditTextTeacherPres) {

            LogUtil.i(App.tag, "teach:" + getInput(objectTeach));
        }


        String postCode = getInput(editTextZip);
        LogUtil.i(App.tag, "postCode:" + postCode);

        boolean isgraduate = checkBoxIsGraduate.isChecked();

        LogUtil.i(App.tag, "isgraduate:" + isgraduate);


        ArrayList<String> selectedIds = new ArrayList<>();
        if (isgraduate) {
            selectedIds.add("15");
        } else {

            if (checkBoxSelectAllYearLevel.isChecked()) {
                LogUtil.i(App.tag, "select all level");
                selectedIds.add("1");
                LogUtil.e(App.tag, "=========");

                for (CheckBox cx : arrayListCheckBoxSecondArys) {
                    if (cx.isChecked()) {
                        String tag = (String) cx.getTag();
                        selectedIds.add(tag.replace("tagPre", ""));
                        LogUtil.i(App.tag, "select tag pre:" + tag);
                    }
                }


            } else {

                for (CheckBox cx : arrayListCheckBoxTeachPrsPrimary) {
                    if (cx.getId() != checkBoxSelectAllYearLevel.getId()) {
                        if (cx.isChecked()) {
                            String tag = (String) cx.getTag();
                            selectedIds.add(tag.replace("tagPre", ""));
                            LogUtil.i(App.tag, "select tag pre:" + tag);
                        }

                    }
                }

                LogUtil.e(App.tag, "=========");

                for (CheckBox cx : arrayListCheckBoxSecondArys) {
                    if (cx.isChecked()) {
                        String tag = (String) cx.getTag();
                        selectedIds.add(tag.replace("tagPre", ""));
                        LogUtil.i(App.tag, "select tag pre:" + tag);
                    }
                }


                LogUtil.i(App.tag, "printf:" + selectedIds);

            }


        }

        String selfDesc = getInput(editTextSelfDesc);

        LogUtil.i(App.tag, "selfdesc:" + selfDesc);

        LogUtil.e(App.tag, "=========================");


        try {
            final JSONObject jsonObject = new JSONObject();

            jsonObject.put("first_name", firstName);
            jsonObject.put("last_name", lastName);
            jsonObject.put("form_lodged", employer);
            jsonObject.put("register_type", registerdLocation);
            jsonObject.put("register_number", teacherNumber);

            jsonObject.put("register_state", registedState);
            jsonObject.put("phone", phoneNumber);
            jsonObject.put("teaching_preference", teachPre);


            JSONArray subjectArray = new JSONArray();

            for (EditText editTextSubject : arrayListEditTextTeacherPres) {
                subjectArray.put(getInput(editTextSubject));
            }

            jsonObject.put("subjects_preference", subjectArray);

            jsonObject.put("postcode", getInput(editTextZip));

            //lat
            //lng


            JSONArray levelExperence = new JSONArray();

            for (String selectId : selectedIds) {

                levelExperence.put(Integer.parseInt(selectId));

            }


            jsonObject.put("level_experience", levelExperence);


            jsonObject.put("year_experience", (String) spinnerTeachyear.getSelectedItem());
            jsonObject.put("introduction", getInput(editTextSelfDesc));


            LogUtil.e(App.tag, "request json:==============");
            LogUtil.e(App.tag, jsonObject.toString());
            LogUtil.e(App.tag, "request json:==============");


            reSetTask();
            baseTask = new BaseTask(EditProfileActivity.this, new NetCallBack() {

                @Override
                public void onPreCall(BaseTask baseTask) {

                    baseTask.showDialogForSelf(true);
                }

                @Override
                public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                    NetResult netResult = null;

                    try {
                        netResult = NetInterface.uploadProfile(EditProfileActivity.this, jsonObject);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return netResult;
                }

                @Override
                public void onFinish(NetResult result, BaseTask baseTask) {
                    baseTask.hideDialogSelf();
                    if (null == result) {
                        showNotifyTextIn5Seconds(R.string.error_net);
                    } else {
                        if (result.isOk()) {

                            App.getApp().setUser(((User) result.getData()[0]));
                            showNotifyTextIn5Seconds("Success");

                            if (isFromRegist) {
                                Tool.startActivity(EditProfileActivity.this, MainActivity.class);
                                finish();
                            } else {
                                setResult(RESULT_OK);
                                finish();
                            }
                        } else {
                            showNotifyTextIn5Seconds(result.getMessage());
                        }

                    }

                }
            });

            baseTask.execute(new HashMap());

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();


    }

    @Override
    public void initView() {


        User user = App.getApp().getUser();


        if (null != user) {
            editTextFirstName.setText(user.getFirst_name());
            editTextLastName.setText(user.getLast_name());

            if (!StringUtils.isEmpty(user.getAvatar())) {

                String avatarUrl = App.getApp().getUser().getAvatar();
//                Picasso.with(App.getApp()).invalidate(avatarUrl);//不要随便调用invalidate 方法,这个方法相当于从内存中删除缓存,特殊用途的时候才用
                Picasso.with(App.getApp()).load(avatarUrl)
                        .config(Bitmap.Config.RGB_565)
                        .placeholder(R.drawable.bg_header_default)
                        .into(roundImageView);
            }
        }

        //========================================================================
        String[] mItems = getResources().getStringArray(R.array.profile_lod_form);
        final String values[] = {"1", "2"};
        final SpninnerAdapter spninnerAdapter = new SpninnerAdapter(this, values, mItems);
        spninnerLodEmployer.setAdapter(spninnerAdapter);
        spninnerLodEmployer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1) {
                    textViewNotifyLabel.setVisibility(View.VISIBLE);
                } else {
                    textViewNotifyLabel.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (null != user) {

            int formLoad = -1;
            try {
                formLoad = Integer.parseInt(user.getForm_lodged());
            } catch (Exception e) {
            }
            if (-1 != formLoad) {
                spninnerLodEmployer.setSelection(formLoad - 1);
            }
        }

        //========================================================================


        String[] mItemsRegisted = getResources().getStringArray(R.array.profile_registed);
        String valuesRegisted[] = {"1", "2", "3"};

        SpninnerAdapter spninnerAdapterRegisted = new SpninnerAdapter(this, valuesRegisted, mItemsRegisted);
        spinnerRegisterd.setAdapter(spninnerAdapterRegisted);

        if (null != user) {

            int registerType = -1;
            try {
                registerType = Integer.parseInt(user.getRegister_type());
            } catch (Exception e) {
            }
            if (-1 != registerType) {
                spinnerRegisterd.setSelection(registerType - 1);
            }

        }
        //========================================================================


        editTextTeacherRegistNumber.setText(user.getRegister_number());


        //========================================================================
        String[] mItemsRegistedState = getResources().getStringArray(R.array.profile_registed_state);
        String valuesRegistedState[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        SpninnerAdapter spninnerAdapterRegistedState = new SpninnerAdapter(this, valuesRegistedState, mItemsRegistedState);
        spinnerRegisterdState.setAdapter(spninnerAdapterRegistedState);

        if (null != user) {

            int registerState = -1;
            try {
                registerState = Integer.parseInt(user.getRegister_state());
            } catch (Exception e) {
            }
            if (-1 != registerState) {
                spinnerRegisterd.setSelection(registerState - 1);
            }

        }

        //========================================================================

        editTextPhoneNumber.setText(user.getPhone());

        //========================================================================

        String teacherPre = user.getTeaching_preference();

        RadioButton rb = (RadioButton) radioGroupTeacherPreference.findViewWithTag((teacherPre));
        if (null != rb) {
            rb.setChecked(true);
        }

        //========================================================================

        ArrayList<String> subjects = user.getSubjects_preference();

        if (!ListUtiles.isEmpty(subjects)) {

            for (String subject : subjects) {

                final View teacherPreHaved = layoutInflater.inflate(R.layout.item_teach_pre, null);

                linearLayoutTaughts.addView(teacherPreHaved);

                final EditText editTextPre = (EditText) teacherPreHaved.findViewWithTag("editTextTeachPre");
                editTextPre.setText(subject);
                arrayListEditTextTeacherPres.add(editTextPre);
                View buttonDel = teacherPreHaved.findViewWithTag("buttonDeleteTeachPre");
                buttonDel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        linearLayoutTaughts.removeView(teacherPreHaved);
                        arrayListEditTextTeacherPres.remove(editTextPre);
                        LogUtil.i(App.tag, "click del");
                    }
                });

            }
        }


        //========================================================================

        editTextZip.setText(user.getPost_code());

        //========================================================================

        checkBoxIsGraduate.setChecked(isSelectPreYear(15, user));

        //========================================================================

        String[] mItemsTeachYear = getResources().getStringArray(R.array.profile_number_teacher_years);
        String valuesTeacher[] = {"1", "2", "3", "4", "5", "6"};
        SpninnerAdapter spninnerAdapterTeacherYear = new SpninnerAdapter(this, valuesTeacher, mItemsTeachYear);
        spinnerTeachyear.setAdapter(spninnerAdapterTeacherYear);


        //========================================================================


        final CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (buttonView.getId() == R.id.checkBoxSelectAllYearLevel) {
                    checkAllYearLevel(isChecked, R.id.checkBoxSelectAllYearLevel);
                } else {
                    checkBoxSelectAllYearLevel.setOnCheckedChangeListener(null);
                    if (!isChecked) {
                        checkBoxSelectAllYearLevel.setChecked(false);
                    }
                    checkBoxSelectAllYearLevel.setOnCheckedChangeListener(this);
                }
            }
        };


        for (int i = 1; i <= 8; i++) {
            CheckBox cbox = (CheckBox) linearLayoutTeacherPre.findViewWithTag("tagPre" + String.valueOf(i));

            cbox.setChecked(isSelectPreYear(i, user));
            cbox.setOnCheckedChangeListener(onCheckedChangeListener);
            arrayListCheckBoxTeachPrsPrimary.add(cbox);

        }

        for (int i = 9; i <= 14; i++) {
            CheckBox cbox = (CheckBox) linearLayoutTeacherPre.findViewWithTag("tagPre" + String.valueOf(i));
            cbox.setChecked(isSelectPreYear(i, user));
            arrayListCheckBoxSecondArys.add(cbox);
        }

        //========================================================================


        int teacheryear = -1;

        try {
            teacheryear = Integer.parseInt(user.getYear_experience());
        } catch (Exception e) {
        }

        if (-1 != teacheryear) {
            spinnerTeachyear.setSelection(teacheryear - 1);
        }


        //========================================================================

        editTextSelfDesc.setText(user.getIndroduction());


        LogUtil.i(App.tag, "find teach pre" + arrayListCheckBoxTeachPrsPrimary.size());


    }

    private boolean isSelectPreYear(int i, User user) {

        if (null != user) {
            ArrayList<String> exps = user.getLevel_experience();
            if (!ListUtiles.isEmpty(exps)) {

                for (String tx : exps) {
                    if (String.valueOf(i).equals(tx)) {
                        return true;
                    }
                }

            }

        }
        return false;
    }


    private void checkAllYearLevel(boolean isCheck, int id) {
        for (CheckBox checkBox : arrayListCheckBoxTeachPrsPrimary) {
            if (checkBox.getId() != id) {
                checkBox.setChecked(isCheck);
            }

        }
    }

    private boolean isAllyearLevelCheck() {

        for (CheckBox checkBox : arrayListCheckBoxTeachPrsPrimary) {

            if (!checkBox.isChecked()) {

                return false;
            }
        }

        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        String mFileName;
        if (requestCode == SELECT_PIC && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getDataColumn(getApplicationContext(), data.getData(), null, null);
                PhotoLibUtils.copyFile(mFileName, sharedpreferencesUtil.getImageTempNameString2());
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
                cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
            }
        }
        if (requestCode == SELECT_PIC_KITKAT && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getPath(getApplicationContext(), data.getData());
                String newPath = sharedpreferencesUtil.getImageTempNameString2();
                PhotoLibUtils.copyFile(mFileName, newPath);
                Uri uri = sharedpreferencesUtil.getImageTempNameUri();
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
                cropImageUri(uri, outoutUri, outputX, outputY, CROP_BIG_PICTURE);

            }
        }
        if (requestCode == TAKE_BIG_PICTURE && resultCode == RESULT_OK) {
            Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
            cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
        }


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                String imgURI = (String) result.getUri().getPath();//.getStringExtra("URI");
                onCropImgSuccess(imgURI);
//                ((ImageView) findViewById(R.id.quick_start_cropped_image)).setImageURI(result.getUri());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }

        }


        if (requestCode == CROP_BIG_PICTURE && resultCode == Activity.RESULT_OK) {
            if (sharedpreferencesUtil.getImageTempNameUri() != null) {
                Uri result = sharedpreferencesUtil.getImageTempNameUriCroped();
                App.firstRun();
                onCropImgSuccess(result.getPath());
            }
        }
    }


    @OnClick(R.id.textViewDone)
    public void onProfieDone() {

        if (checkValue()) {
            updateInfo();
        }

//        finish();
    }


    @OnClick(R.id.viewClickDone)
    public void onClickDone() {
        if (checkValue()) {
            updateInfo();
        }
    }


    @OnClick(R.id.viewBack)
    public void onBackCLick() {
        finish();
    }


    private boolean checkValue() {

        String firstName = getInput(editTextFirstName);

        if (StringUtils.isEmpty(firstName)) {
            Tool.showMessageDialog("Firstname is empty!", this);
            return false;
        }

        String lastName = getInput(editTextLastName);

        if (StringUtils.isEmpty(lastName)) {
            Tool.showMessageDialog("Lastname is empty!", this);
            return false;
        }


        if (-1 == spninnerLodEmployer.getSelectedItemPosition()) {
            Tool.showMessageDialog("Please confirm to lodged your application form!", this);
            return false;
        }


        if (StringUtils.isEmpty(getInput(editTextTeacherRegistNumber))) {
            Tool.showMessageDialog("Teacher registration number is empty!", this);
            return false;
        }


        if (-1 == spinnerRegisterd.getSelectedItemPosition()) {
            Tool.showMessageDialog("Please confirm registered location!", this);
            return false;
        }


        if (-1 == spinnerRegisterdState.getSelectedItemPosition()) {
            Tool.showMessageDialog("Please confirm registered state!", this);
            return false;
        }

        if (StringUtils.isEmpty(getInput(editTextPhoneNumber))) {

            Tool.showMessageDialog("Please input phone number!", this);
            return false;
        }

        if (-1 == radioGroupTeacherPreference.getCheckedRadioButtonId()) {
            Tool.showMessageDialog("Please confirm teaching preferences!", this);
            return false;
        }

        if (ListUtiles.isEmpty(arrayListEditTextTeacherPres)) {
//            Tool.showMessageDialog("Please add teaching subject!", this);
            return true;
        } else {

            for (EditText editTextInput : arrayListEditTextTeacherPres) {
                if (StringUtils.isEmpty(getInput(editTextInput))) {
                    Tool.showMessageDialog("Teaching object can not be empty!", this);
                    return false;
                }

            }

        }

        String zip = getInput(editTextZip);

        if (StringUtils.isEmpty(zip) || zip.length() != 4) {
            Tool.showMessageDialog("Please input 4 numbers post code!", this);
            return false;
        }


        if (checkBoxIsGraduate.isChecked()) {


        } else {

            boolean isSelectLevelExperence = false;

            for (CheckBox cx : arrayListCheckBoxTeachPrsPrimary) {
                if (cx.isChecked()) {
                    isSelectLevelExperence = true;
                    break;
                }
            }
            if (isSelectLevelExperence == false) {

                for (CheckBox cx : arrayListCheckBoxSecondArys) {
                    if (cx.isChecked()) {
                        isSelectLevelExperence = true;
                        break;
                    }
                }
            }

            if (isSelectLevelExperence == false) {
                Tool.showMessageDialog("Please select level experence.", this);
                return false;
            }

        }


        if (-1 == spinnerTeachyear.getSelectedItemPosition()) {

            Tool.showMessageDialog("Please select number of years teaching experence", this);
            return false;
        }

        return true;

    }


    public void onCropImgSuccess(String picPath) {

        if (!StringUtils.isEmpty(picPath)) {
            File selectFile = new File(picPath);
            if (null != selectFile && selectFile.exists()) {
                try {

                    upLoadHeaderImg(EditProfileActivity.this, picPath, new OnUploadCallBack() {
                        @Override
                        public void onUploadEnd(NetResult netResult, Context context) {
                            if (null != context) {
                                if (null != netResult) {

                                    String avatarUrl = (String) netResult.getData()[0];
                                    LogUtil.e(App.tag, "fileurl:" + avatarUrl);


                                    User currentUser = App.getApp().getUser();
                                    currentUser.setAvatar(avatarUrl);
                                    App.getApp().setUser(currentUser);


                                    Picasso.with(App.getApp()).invalidate(avatarUrl);

                                    Picasso.with(App.getApp()).load(avatarUrl)
                                            .config(Bitmap.Config.RGB_565)
                                            .networkPolicy(NetworkPolicy.NO_CACHE)
                                            .into(roundImageView);

                                    roundImageView.invalidate();

                                } else {
                                    Tool.showMessageDialog("Net error!", (Activity) context);
                                }
                            }
                        }
                    });

                } catch (Exception e) {
                    LogUtil.e(App.tag, "compress picture fail!");
                }
            } else {
                LogUtil.i(App.tag, "file not exist!");
            }

        }


    }


    @OnClick(R.id.roundImgView)
    public void onImgClick() {

        PhotoSelectDialog photoSelectDialog = new PhotoSelectDialog(this);
        photoSelectDialog.setOnAddCatchListener(new PhotoSelectDialog.OnPhotoSelectionActionListener() {
            @Override
            public void onTakePhotoClick() {

                onTakePhtoClick();

            }

            @Override
            public void onChoosePhotoClick() {
                onChosePhtoClick();
            }

            @Override
            public void onDismis() {

            }
        });
        photoSelectDialog.show();

    }

}
