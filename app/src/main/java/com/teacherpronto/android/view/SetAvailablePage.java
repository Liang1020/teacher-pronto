package com.teacherpronto.android.view;

import android.graphics.Color;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.Tool;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;
import com.teacherpronto.android.activity.MainActivity;
import com.teacherpronto.android.bean.Availability;
import com.teacherpronto.android.bean.AvailableDate;
import com.teacherpronto.android.net.NetInterface;
import com.teacherpronto.android.util.CalendarUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by clia5292 on 2016/4/25.
 */
public class SetAvailablePage extends BasePage implements View.OnClickListener {

    //api 0 availabile all day      1 am na      2  pm na     3 na all day
    private static final String TAG = "AVAILABLEPAGE";

    public static final int AV_ALL_DAY = 0;
    public static final int NA_AM_JOB_PM = 1;
    public static final int NA_PM_JOB_AM = 2;
    public static final int NA_ALL_DAY = 3;
    public static final int AV_AM_JOB_PM = 4;
    public static final int AV_PM_JOB_AM = 5;
    public static final int AV_AM = 6;
    public static final int AV_PM = 7;
    public static final int JOB_ALL_DAY = 8;

    private TextView mCurrentDate;
    private TextView mBottomDate;
    private ImageButton btnNextMonth;
    private ImageButton btnPreMonth;
    private CheckBox btnAM;
    private CheckBox btnPM;
    private ImageButton imgBtnRepeat;
    private LinearLayout mLinearLayoutBottom;

    private static AvailableDate mShowDate;
    private int mMonthDays;
    private int firstDayWeek;
    private String[][] mDayArray;
    private String key = null;
    private int repeatType = 0;

    private ArrayList<TextView> textViews;
    private ArrayList<ImageView> imageViews;
    private ArrayList<Integer> mStates;
    private ArrayList<Integer> mAvailabilities;

    private boolean refreshDataFromRemote = true;
    private boolean isFirst = true;

    private ArrayList<Availability> avail = new ArrayList();

    private Handler mHandler = new Handler();
    private Thread mThread;
    private Runnable mRunnable = new Runnable() {
        public void run() {
            refreshView();
        }
    };

    private ImageView imgView;
    private int pos, availability, count;


    private int state_pos = -1;

    private Animation animationFadeIn;
    private Animation animationFadeOut;

    private ImageView imageViewCover;

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.imgBtnNextMonth:
                rightSlide();
                break;
            case R.id.imgBtnPreMonth:
                leftSlide();
                break;
            case R.id.btnAM:

                LogUtil.i(App.tag, "btnPM:" + "onClick");
                if(state_pos == -1){
                    Tool.showMessageDialog("Please select date first!", mainActivity);
                    btnAM.setChecked(false);
                    btnPM.setChecked(false);
                    break;
                }
                setCheckedValue();

                LogUtil.i(TAG, "PM_AM" + "_" + String.valueOf(btnPM.isChecked()) + " " + String.valueOf(btnAM.isChecked()));

                if (btnPM.isChecked()) {
                    if (((CompoundButton) v).isChecked()) {
                        mAvailabilities.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_AM);
                        refreshView();

                        if (!hasJob(state_pos)) {
                            imgView.setImageResource(R.drawable.available_am_p);
                            mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_AM);
                        } else {
                            if (mStates.get(state_pos) == NA_AM_JOB_PM) {
                                imgView.setImageResource(R.drawable.available_am_job_pm_p);
                                mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_AM_JOB_PM);
                            } else {
                                changeDateState(state_pos);
                            }
                        }

                        am_selected();
                        btnAM.setChecked(false);
                    } else {
                        mAvailabilities.set(Integer.valueOf(imgView.getTag().toString()) - 25, NA_ALL_DAY);
                        refreshView();

                        if (!hasJob(state_pos)) {
                            imgView.setImageResource(R.drawable.na_whole_day_p);
                            mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, NA_ALL_DAY);
                        } else {
                            if (mStates.get(state_pos) == AV_AM_JOB_PM) {
                                imgView.setImageResource(R.drawable.na_am_job_pm_p);
                                mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, NA_AM_JOB_PM);
                            } else {
                                changeDateState(state_pos);
                            }
                        }

                        am_unselected();
                        btnAM.setChecked(true);
                    }
                } else {
                    if (btnAM.isChecked()) {
                        mAvailabilities.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_ALL_DAY);
                        refreshView();

                        if (!hasJob(state_pos)) {
                            imgView.setImageResource(R.drawable.available_whole_day_p);
                            mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_ALL_DAY);
                        } else {
                            if (mStates.get(state_pos) == NA_AM_JOB_PM) {
                                imgView.setImageResource(R.drawable.available_am_job_pm_p);
                                mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_AM_JOB_PM);
                            } else {
                                changeDateState(state_pos);
                            }

                        }

                        am_selected();
                        btnAM.setChecked(false);
                    } else {
                        mAvailabilities.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_PM);
                        refreshView();

                        if (!hasJob(state_pos)) {
                            imgView.setImageResource(R.drawable.available_pm_p);
                            mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_PM);
                        } else {
                            if (mStates.get(state_pos) == AV_AM_JOB_PM) {
                                imgView.setImageResource(R.drawable.na_am_job_pm_p);
                                mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, NA_AM_JOB_PM);
                            } else {
                                changeDateState(state_pos);
                            }
                        }
                        am_unselected();
                        btnAM.setChecked(true);
                    }
                }

                LogUtil.i(TAG, "state:" + mStates.get(state_pos).toString());
                key = mShowDate.getMonth() + "_" + mShowDate.getYear();
                mShowDate.saveState(key, state_pos, mStates.get(state_pos));
                mShowDate.saveAvailability(key, state_pos, mAvailabilities.get(state_pos));

                submitAvailability();
                break;

            case R.id.btnPM:
                LogUtil.i(App.tag, "btnPM:" + "onClick");
                if(state_pos == -1){
                    btnPM.setChecked(false);
                    btnAM.setChecked(false);
                    Tool.showMessageDialog("Please select date first!", mainActivity);
                    break;
                }

                setCheckedValue();
                LogUtil.i(TAG, "PM_AM" + String.valueOf(btnPM.isChecked()) + " " + String.valueOf(btnAM.isChecked()));
                if (btnAM.isChecked()) {
                    if (btnPM.isChecked()) {
                        mAvailabilities.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_PM);
                        refreshView();

                        if (!hasJob(state_pos)) {
                            imgView.setImageResource(R.drawable.available_pm_p);
                            mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_PM);
                        } else {
                            if (mStates.get(state_pos) == NA_PM_JOB_AM) {
                                imgView.setImageResource(R.drawable.available_pm_job_am_p);
                                mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_PM_JOB_AM);
                            } else {
                                changeDateState(state_pos);
                            }
                        }

                        pm_selected();
                        btnPM.setChecked(false);
                    } else {
                        mAvailabilities.set(Integer.valueOf(imgView.getTag().toString()) - 25, NA_ALL_DAY);
                        refreshView();
                        if (!hasJob(state_pos)) {
                            imgView.setImageResource(R.drawable.na_whole_day_p);
                            mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, NA_ALL_DAY);
                        } else {
                            if (mStates.get(state_pos) == AV_PM_JOB_AM) {
                                imgView.setImageResource(R.drawable.na_pm_job_am_p);
                                mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, NA_PM_JOB_AM);
                            } else {
                                changeDateState(state_pos);
                            }
                        }

                        pm_unselected();
                        btnPM.setChecked(true);
                    }
                } else {
                    if (btnPM.isChecked()) {
                        mAvailabilities.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_ALL_DAY);
                        refreshView();

                        if (!hasJob(state_pos)) {
                            imgView.setImageResource(R.drawable.available_whole_day_p);
                            mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_ALL_DAY);
                        } else {
                            if (mStates.get(state_pos) == NA_PM_JOB_AM) {
                                imgView.setImageResource(R.drawable.available_pm_job_am_p);
                                mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_PM_JOB_AM);
                            } else {
                                changeDateState(state_pos);
                            }
                        }

                        pm_selected();
                        btnPM.setChecked(false);
                    } else {
                        mAvailabilities.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_AM);
                        refreshView();

                        if (!hasJob(state_pos)) {
                            imgView.setImageResource(R.drawable.available_am_p);
                            mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, AV_AM);
                        } else {
                            if (mStates.get(state_pos) == AV_PM_JOB_AM) {
                                imgView.setImageResource(R.drawable.na_pm_job_am_p);
                                mStates.set(Integer.valueOf(imgView.getTag().toString()) - 25, NA_PM_JOB_AM);
                            } else {
                                changeDateState(state_pos);
                            }
                        }

                        pm_unselected();
                        btnPM.setChecked(true);
                    }
                }

                key = mShowDate.getMonth() + "_" + mShowDate.getYear();
                mShowDate.saveState(key, state_pos, mStates.get(state_pos));
                mShowDate.saveAvailability(key, state_pos, mAvailabilities.get(state_pos));
                LogUtil.i(TAG, "state:" + mStates.get(state_pos).toString());
                submitAvailability();
                break;
            case R.id.imgButtonRepeat:

                if (null == imgView) {
                    Tool.showMessageDialog("Please select date first.", mainActivity);
                    return;
                }

                DialogRepeat dialogRepeat = new DialogRepeat(mainActivity, Integer.valueOf(imgView.getTag().toString()));
                dialogRepeat.setOnDoneClickListener(new DialogRepeat.OnDoneClickListener() {
                    @Override
                    public void onDoneClick(String repeatTag) {
                        switch (repeatTag) {

                            case "Weekly":
                                repeatType = 1;
                                pos = Integer.valueOf(imgView.getTag().toString()) - 25;
                                if (textViews.get(pos).getText().equals("")) {
                                    return;
                                }
                                availability = mStates.get(pos);
                                for (int i = mShowDate.getMonth(); i <= 12; i++) {
                                    count = 0;
                                    for (int j = pos; j < 25; ) {
                                        key = i + "_" + mShowDate.getYear();
                                        mShowDate.saveState(key, pos + 5 * count, availability);
                                        mShowDate.saveAvailability(key, pos + 5 * count, availability);
                                        int p = pos - 5;
                                        while (p >= 0 && i > mShowDate.getMonth()) {
                                            mShowDate.saveState(key, p, availability);
                                            mShowDate.saveAvailability(key, p, availability);
                                            p -= 5;

                                        }
                                        count++;
                                        j += 5;

                                    }
                                }
                                update();
                                submitAvailability();
                                repeatType = 0;
                                break;

                            case "Fornightly":
                                repeatType = 2;
                                pos = Integer.valueOf(imgView.getTag().toString()) - 25;
                                if (textViews.get(pos).getText().equals("")) {
                                    return;
                                }
                                availability = mStates.get(pos);
                                for (int i = mShowDate.getMonth(); i <= 12; i++) {
                                    count = 0;
                                    for (int j = pos; j < 25; ) {

                                        key = i + "_" + mShowDate.getYear();
                                        mShowDate.saveState(key, pos + 10 * count, availability);
                                        mShowDate.saveAvailability(key, pos + 10 * count, availability);
                                        int p = pos - 10;
                                        while (p >= 0 && i > mShowDate.getMonth()) {
                                            mShowDate.saveState(key, p, availability);
                                            mShowDate.saveAvailability(key, p, availability);
                                            p -= 10;
                                        }

                                        count++;
                                        j += 10;
                                    }
                                    if (CalendarUtil.getMonthDays(mShowDate.getYear(), i) == 31) {
                                        if (CalendarUtil.getWeek(31, i, mShowDate.getYear()) == 5 ||
                                                CalendarUtil.getWeek(30, i, mShowDate.getYear()) == 5 ||
                                                CalendarUtil.getWeek(29, i, mShowDate.getYear()) == 5 ||
                                                CalendarUtil.getWeek(28, i, mShowDate.getYear()) == 5) {
                                            if (pos - 5 > 0) {
                                                pos -= 5;
                                            } else {
                                                pos += 5;
                                            }
                                        }
                                    } else {
                                        if (CalendarUtil.getWeek(30, i, mShowDate.getYear()) == 5 ||
                                                CalendarUtil.getWeek(29, i, mShowDate.getYear()) == 5 ||
                                                CalendarUtil.getWeek(28, i, mShowDate.getYear()) == 5) {
                                            if (pos - 5 > 0) {
                                                pos -= 5;
                                            } else {
                                                pos += 5;
                                            }
                                        }
                                    }

                                }
                                update();
                                submitAvailability();
                                repeatType = 0;
                                break;
                            case "Monthly":
                                repeatType = 3;
                                pos = Integer.valueOf(imgView.getTag().toString()) - 25;
                                if (textViews.get(pos).getText().equals("")) {
                                    return;
                                }
                                availability = mStates.get(pos);
                                for (int i = mShowDate.getMonth(); i <= 12; i++) {
                                    key = i + "_" + mShowDate.getYear();
                                    mShowDate.saveState(key, pos, availability);
                                    mShowDate.saveAvailability(key, pos, availability);

                                }
                                update();
                                submitAvailability();
                                repeatType = 0;
                                break;


                        }
                    }
                });
                dialogRepeat.show();
                break;
        }

    }

    public SetAvailablePage(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.rootView = View.inflate(mainActivity, R.layout.page_set_avaliable, null);

        this.imageViewCover = (ImageView) rootView.findViewById(R.id.imageViewCover);

        animationFadeIn = AnimationUtils.loadAnimation(mainActivity, R.anim.abc_fade_in);
        animationFadeOut = AnimationUtils.loadAnimation(mainActivity, R.anim.abc_fade_out);

        animationFadeIn.setDuration(200);
        animationFadeOut.setDuration(200);

        mShowDate = new AvailableDate(mainActivity);
        mStates = new ArrayList<>();
        mAvailabilities = new ArrayList<>();


    }


    @Override
    public void onShow() {
        //onShow 会被调用(当在主activity显示当前page的时候)
        //注意:根据实际情况判断是否需要 调用requestAvailability
        requestAvailability();

        if (SharePersistent.getBoolean(mainActivity, "see_guild_ava") == false) {
            imageViewCover.setVisibility(View.VISIBLE);
            imageViewCover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharePersistent.saveBoolean(mainActivity, "see_guild_ava", true);
                    imageViewCover.setVisibility(View.GONE);
                }
            });
        } else {
            imageViewCover.setVisibility(View.GONE);
        }

        mBottomDate = (TextView) findViewById(R.id.textViewBottomDate);
        mBottomDate.setText("");

    }

    private void fillDate() {
        mMonthDays = CalendarUtil.getMonthDays(mShowDate.getYear(),
                mShowDate.getMonth());
        firstDayWeek = CalendarUtil.getWeekDayFromDate(mShowDate.getYear(),
                mShowDate.getMonth());

        mDayArray = new String[6][7];
        for (int day = 0; day < mMonthDays; day++) {

            int col = (day + firstDayWeek) % 7;
            int row = (day + firstDayWeek) / 7;
            mDayArray[row][col] = day + 1 + "";
        }


        for (int i = 0; i < 25; i++) {
            key = mShowDate.getMonth() + "_" + mShowDate.getYear();
            mStates.add(i, mShowDate.getState(key, i));
            mAvailabilities.add(i, mShowDate.getAvailability(key, i));
        }

    }

    private void initView() {
        mCurrentDate = (TextView) findViewById(R.id.txtViewCurDate);
        btnNextMonth = (ImageButton) findViewById(R.id.imgBtnNextMonth);
        btnPreMonth = (ImageButton) findViewById(R.id.imgBtnPreMonth);


        btnAM = (CheckBox) findViewById(R.id.btnAM);
//        btnAM.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                LogUtil.i(App.tag,"setOnClickListener");
//            }
//        });
//        btnAM.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                LogUtil.i(App.tag,"onCheckedChanged");
//            }
//        });

        btnPM = (CheckBox) findViewById(R.id.btnPM);


        imgBtnRepeat = (ImageButton) findViewById(R.id.imgButtonRepeat);
        mLinearLayoutBottom = (LinearLayout) findViewById(R.id.linearLayoutBottom);

        btnNextMonth.setOnClickListener(this);
        btnPreMonth.setOnClickListener(this);

        btnAM.setOnClickListener(this);
        btnPM.setOnClickListener(this);

        imgBtnRepeat.setOnClickListener(this);

        textViews = new ArrayList<>();
        imageViews = new ArrayList<>();


        for (int i = 0; i < 25; i++) {
            textViews.add((TextView) rootView.findViewWithTag(String.valueOf(i)));
            imageViews.add((ImageView) rootView.findViewWithTag(String.valueOf(i + 25)));
            imageViews.get(i).setSelected(true);
            imageViews.get(i).setOnClickListener(onCalendarClickListener);
            if (textViews.get(i).getText() == "") {
                imageViews.get(i).setVisibility(View.INVISIBLE);
            }
        }

        refreshView();
    }

    View.OnClickListener onCalendarClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag() == null) {
                return;
            } else {
                state_pos = Integer.valueOf(v.getTag().toString()) - 25;
                changeDateState(Integer.valueOf(v.getTag().toString()) - 25);

                LogUtil.i(TAG, "STATE:" + mStates.get(state_pos).toString());
                LogUtil.i(TAG, "AVAIL:" + mAvailabilities.get(state_pos).toString());

                if (mAvailabilities.get(state_pos) == NA_ALL_DAY) {
                    na_selected();
                } else if (mAvailabilities.get(state_pos) == AV_AM) {
                    am_selected();
                    pm_unselected();
                } else if (mAvailabilities.get(state_pos) == AV_PM) {
                    pm_selected();
                    am_unselected();
                } else if (mAvailabilities.get(state_pos) == AV_ALL_DAY) {
                    allday_selected();
                }

            }
        }
    };

    private void refreshView() {
        mCurrentDate.setText(CalendarUtil.getMonthString(mShowDate.getMonth()) + " " + mShowDate.getYear());

        for (int i = 0; i < 5; i++) {
            for (int j = 1; j <= 5; j++) {
                textViews.get(i * 5 + j - 1).setText(mDayArray[i][j]);
            }
        }

        for (int i = 0; i < 25; i++) {
            if (textViews.get(i).getText() == "") {
                imageViews.get(i).setVisibility(View.INVISIBLE);
            } else {
                imageViews.get(i).setVisibility(View.VISIBLE);
            }
            imageViews.get(i).setImageResource(getDateState(mStates.get(i)));
        }

        if (refreshDataFromRemote) {
            LogUtil.i(TAG, "IsFirst:" + isFirst);

            if (!isFirst) {
                for (int i = 0; i < 25; i++) {
                    mStates.set(i, AV_ALL_DAY);
                }
                requestAvailability();
            } else {
                refreshRemoteData();
            }
            isFirst = false;

            refreshDataFromRemote = false;
        }

    }

    private void refreshRemoteData() {

        for (Availability av : avail) {

            if (String.valueOf(mShowDate.getYear()).equals(av.getYear()) && String.valueOf(mShowDate.getMonth()).equals(av.getMonth())) {
                resetButtonAMPM();
                for (int i = 0; i < 25; i++) {
                    if (textViews.get(i).getText().equals(av.getDay())) {

                        if (av.getBooking().equals("") || av.getBooking().equals("0")) {

                            LogUtil.i(TAG, "booking 0");
                            if (av.getAvailability().equals("1")) {
                                mStates.set(i, AV_PM);
                                mAvailabilities.set(i, AV_PM);
                            } else if (av.getAvailability().equals("2")) {
                                mStates.set(i, AV_AM);
                                mAvailabilities.set(i, AV_AM);
                            } else if (av.getAvailability().equals("3")) {
                                mStates.set(i, NA_ALL_DAY);
                                mAvailabilities.set(i, NA_ALL_DAY);
                            } else {
                                mStates.set(i, AV_ALL_DAY);
                                mAvailabilities.set(i, AV_ALL_DAY);
                            }

                        } else if (av.getBooking().equals("1")) {
//                            btnAM.setClickable(false);
                            LogUtil.i(TAG, "booking 1");
                            if (av.getAvailability().equals("1")) {
                                mStates.set(i, AV_PM_JOB_AM);
                                mAvailabilities.set(i, AV_PM);
                            } else if (av.getAvailability().equals("2")) {
                                mStates.set(i, NA_PM_JOB_AM);
                                mAvailabilities.set(i, AV_AM);
                            } else if (av.getAvailability().equals("3")) {
                                mStates.set(i, NA_PM_JOB_AM);
                                mAvailabilities.set(i, NA_ALL_DAY);
                            } else {
                                mStates.set(i, AV_PM_JOB_AM);
                                mAvailabilities.set(i, AV_ALL_DAY);
                            }

                        } else if (av.getBooking().equals("2")) {
//                            btnPM.setClickable(false);
                            LogUtil.i(TAG, "booking 2");
                            if (av.getAvailability().equals("1")) {
                                mStates.set(i, NA_AM_JOB_PM);
                                mAvailabilities.set(i, AV_PM);
                            } else if (av.getAvailability().equals("2")) {
                                mStates.set(i, AV_AM_JOB_PM);
                                mAvailabilities.set(i, AV_AM);
                            } else if (av.getAvailability().equals("3")) {
                                mStates.set(i, NA_AM_JOB_PM);
                                mAvailabilities.set(i, NA_ALL_DAY);
                            } else {
                                mStates.set(i, AV_AM_JOB_PM);
                                mAvailabilities.set(i, AV_ALL_DAY);
                            }

                        } else if (av.getBooking().equals("3")) {

                            LogUtil.i(App.tag, "=======set click false=========");
                            LogUtil.i(TAG, "booking 3");
//                            btnAM.setClickable(false);//????????
//                            btnPM.setClickable(false);

                            mStates.set(i, JOB_ALL_DAY);

                            if (av.getAvailability().equals("1")) {
                                mAvailabilities.set(i, AV_PM);
                            } else if (av.getAvailability().equals("2")) {
                                mAvailabilities.set(i, AV_AM);
                            } else if (av.getAvailability().equals("3")) {
                                mAvailabilities.set(i, NA_ALL_DAY);
                            } else {
                                mAvailabilities.set(i, AV_ALL_DAY);
                            }

                        }

                        imageViews.get(i).setImageResource(getDateState(mStates.get(i)));

                    }
                }
            }
        }

    }

    private void resetButtonAMPM() {
        btnAM.setClickable(true);
        btnPM.setClickable(true);
    }

    private boolean hasJob(int pos) {
        int state = mStates.get(pos);
        if (state == NA_AM_JOB_PM || state == NA_PM_JOB_AM || state == AV_AM_JOB_PM || state == AV_PM_JOB_AM || state == JOB_ALL_DAY) {
            return true;
        }
        return false;
    }

    public void leftSlide() {
        refreshDataFromRemote = true;

        if (mShowDate.getMonth() == 1) {
            mShowDate.setMonth(12);
            mShowDate.setYear(mShowDate.getYear() - 1);
        } else {
            mShowDate.setMonth(mShowDate.getMonth() - 1);
        }
        update();
    }


    public void refreshCurrentDateView() {
        refreshDataFromRemote = true;
        update();
    }


    public void rightSlide() {
        refreshDataFromRemote = true;

        if (mShowDate.getMonth() == 12) {
            mShowDate.setMonth(1);
            mShowDate.setYear(mShowDate.getYear() + 1);
        } else {
            mShowDate.setMonth(mShowDate.getMonth() + 1);
        }
        update();
    }

    public void update() {
        fillDate();
        mThread = new Thread() {
            public void run() {
                mHandler.post(mRunnable);
            }
        };
        mThread.start();
    }

    public int getDateState(int availability) {
        switch (availability) {
            case NA_ALL_DAY:
                return R.drawable.na_whole_day;
            case NA_AM_JOB_PM:
                return R.drawable.na_am_job_pm;
            case NA_PM_JOB_AM:
                return R.drawable.na_pm_job_am;
            case AV_ALL_DAY:
                return R.drawable.available_whole_day;
            case AV_AM_JOB_PM:
                return R.drawable.available_am_job_pm;
            case AV_PM_JOB_AM:
                return R.drawable.available_pm_job_am;
            case AV_AM:
                return R.drawable.available_am;
            case AV_PM:
                return R.drawable.available_pm;
            case JOB_ALL_DAY:
                return R.drawable.job_whole_day;
            default:
                return -1;
        }
    }

    public int getSelectedDateState(int availability) {
        resetButtonAMPM();
        LogUtil.i(App.tag, "Compare Result"
                + CalendarUtil.CompareDate(Integer.valueOf(textViews.get(state_pos).getText().toString()),
                mShowDate.getMonth(),
                mShowDate.getYear()));
        if(CalendarUtil.CompareDate(Integer.valueOf(textViews.get(state_pos).getText().toString()), mShowDate.getMonth(), mShowDate.getYear()) == -1){
            btnPM.setClickable(false);
            btnAM.setClickable(false);

        }else{
            btnAM.setClickable(true);
            btnPM.setClickable(true);
        }

        switch (availability) {
            case NA_ALL_DAY:
                return R.drawable.na_whole_day_p;
            case NA_AM_JOB_PM:
                btnPM.setClickable(false);
                return R.drawable.na_am_job_pm_p;
            case NA_PM_JOB_AM:
                btnAM.setClickable(false);
                return R.drawable.na_pm_job_am_p;
            case AV_ALL_DAY:
                return R.drawable.available_whole_day_p;
            case AV_AM_JOB_PM:
                btnPM.setClickable(false);
                return R.drawable.available_am_job_pm_p;
            case AV_PM_JOB_AM:
                btnAM.setClickable(false);
                return R.drawable.available_pm_job_am_p;
            case AV_AM:
                return R.drawable.available_am_p;
            case AV_PM:
                return R.drawable.available_pm_p;
            case JOB_ALL_DAY:
                btnAM.setClickable(false);
                btnPM.setClickable(false);
                return R.drawable.job_whole_day_p;
            default:
                return -1;
        }
    }

    private void changeDateState(int pos) {
        for (int i = 0; i < 25; i++) {
            imageViews.get(i).setImageResource(getDateState(mStates.get(i)));
        }
        mLinearLayoutBottom.setVisibility(View.VISIBLE);
        imgView = imageViews.get(pos);

        imgView.setImageResource(getSelectedDateState(mStates.get(pos)));
        if (textViews.get(pos).getText().toString() == "") {
            mBottomDate.setText(CalendarUtil.getMonthString(mShowDate.getMonth()) + " " + mShowDate.getYear());
        } else {
            mBottomDate.setText(CalendarUtil.getWeekString(Integer.valueOf(textViews.get(pos).getText().toString()), mShowDate.getMonth(), mShowDate.getYear()) + " " +
                    textViews.get(pos).getText() + " " + CalendarUtil.getMonthString(mShowDate.getMonth()) + " " + mShowDate.getYear());
        }
    }

    @Override
    public void onDestroy() {
        mThread.currentThread().interrupt();
        mThread = null;

        for (int i = 0; i < 25; i++) {
            key = mShowDate.getMonth() + "_" + mShowDate.getYear();
            mShowDate.saveState(key, i, mStates.get(i));
        }
    }

    private void setCheckedValue() {
        if (mAvailabilities.get(state_pos) == NA_ALL_DAY) {
            na_selected();
            btnAM.setChecked(true);
            btnPM.setChecked(true);
        } else if (mAvailabilities.get(state_pos) == AV_AM) {
            am_selected();
            pm_unselected();
            btnAM.setChecked(false);
            btnPM.setChecked(true);
        } else if (mAvailabilities.get(state_pos) == AV_PM) {
            pm_selected();
            am_unselected();
            btnPM.setChecked(false);
            btnAM.setChecked(true);
        } else if (mAvailabilities.get(state_pos) == AV_ALL_DAY) {
            allday_selected();
            btnAM.setChecked(false);
            btnPM.setChecked(false);
        }
    }

    private void am_selected() {
        btnAM.setBackgroundResource(R.drawable.shape_btn_white);
        btnAM.setTextColor(Color.BLACK);
    }

    private void pm_selected() {
        btnPM.setBackgroundResource(R.drawable.shape_btn_white);
        btnPM.setTextColor(Color.BLACK);
    }

    private void am_unselected() {
        btnAM.setBackgroundResource(R.drawable.shape_btn_black);
        btnAM.setTextColor(Color.WHITE);
    }

    private void pm_unselected() {
        btnPM.setBackgroundResource(R.drawable.shape_btn_black);
        btnPM.setTextColor(Color.WHITE);
    }

    private void allday_selected() {
        btnAM.setBackgroundResource(R.drawable.shape_btn_white);
        btnPM.setBackgroundResource(R.drawable.shape_btn_white);
        btnAM.setTextColor(Color.BLACK);
        btnPM.setTextColor(Color.BLACK);
    }

    private void na_selected() {
        btnAM.setBackgroundResource(R.drawable.shape_btn_black);
        btnPM.setBackgroundResource(R.drawable.shape_btn_black);
        btnAM.setTextColor(Color.WHITE);
        btnPM.setTextColor(Color.WHITE);
    }

    private void requestAvailability() {

        mainActivity.reSetTask();

        mainActivity.baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.getAvailability(mainActivity, NetInterface.buildCustomizedObject(paramMap));
                } catch (Exception e) {
                    LogUtil.e(TAG, "error:" + e.getLocalizedMessage());
                }

                return netResult;
            }


            @Override
            public void onCanCell(BaseTask baseTask) {
                super.onCanCell(baseTask);

                LogUtil.i(App.tag, "cancel dialog");
                baseTask.hideDialogSelf();
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != result) {


                    if (result.isValidateError()) {

//                        {status": 401, "message": "Unauthorized"}    token认证失败
//                        {"status": 401, message": "invalid_credentials"}   login用户名密码认证失败
//                        {"status": 422, "message": "validation_failed", "error": "{validation failed message here}"}

                        App.logout2Login(mainActivity, result.getCode() + "," + result.getMessage());
                        return;


                    } else if (result.isOk()) {
                        avail = (ArrayList<Availability>) result.getData()[0];
                        if (isFirst) {
                            fillDate();
                            initView();
                            isFirst = false;
                        } else if (needRefresh == true) {
                            refreshCurrentDateView();
                            needRefresh = false;
                        }

                        refreshRemoteData();

                    } else {
                        Tool.showMessageDialog(result.getMessage(), mainActivity);
                    }
                } else {
                    Tool.showMessageDialog("Net error", mainActivity);
                }

            }
        }, mainActivity);

        HashMap<String, String> hmap = new HashMap<String, String>();

        hmap.put("api_token", App.getApp().getToken());
        hmap.put("year", String.valueOf(mShowDate.getYear()));

        mainActivity.baseTask.execute(hmap);

    }

    private void submitAvailability() {

        mainActivity.reSetTask();

        mainActivity.baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.setAvailability(mainActivity, NetInterface.buildCustomizedObject(paramMap));
                } catch (Exception e) {
                    LogUtil.e(TAG, "error:" + e.getLocalizedMessage());
                }

                return netResult;
            }


            @Override
            public void onCanCell(BaseTask baseTask) {
                super.onCanCell(baseTask);

                LogUtil.i(App.tag, "cancel dialog");
                baseTask.hideDialogSelf();
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != result) {


                    if (result.isValidateError()) {

//                        {status": 401, "message": "Unauthorized"}    token认证失败
//                        {"status": 401, message": "invalid_credentials"}   login用户名密码认证失败
//                        {"status": 422, "message": "validation_failed", "error": "{validation failed message here}"}

                        App.logout2Login(mainActivity, result.getCode() + "," + result.getMessage());
                        return;


                    } else if (result.isOk()) {

                    } else {
                        Tool.showMessageDialog(result.getMessage(), mainActivity);
                    }
                } else {
                    Tool.showMessageDialog("Net error", mainActivity);
                }

            }
        }, mainActivity);

        HashMap<String, String> hmap = new HashMap<String, String>();

        hmap.put("api_token", App.getApp().getToken());
        String date = mShowDate.getYear() + "-" + CalendarUtil.monthFormat(String.valueOf(mShowDate.getMonth()))
                + "-" + CalendarUtil.dayFormat(textViews.get(state_pos).getText().toString());
        hmap.put("date", date);
        String mAva = "0";
        switch (mAvailabilities.get(state_pos)) {
            case 3:
                mAva = "3";
                break;
            case 6:
                mAva = "2";
                break;
            case 7:
                mAva = "1";
                break;
        }
        hmap.put("availability", mAva);
        hmap.put("repeat_type", String.valueOf(repeatType));

        mainActivity.baseTask.execute(hmap);

    }

}


