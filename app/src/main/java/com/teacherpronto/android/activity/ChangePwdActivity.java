package com.teacherpronto.android.activity;

import android.os.Bundle;
import android.widget.EditText;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;
import com.teacherpronto.android.net.NetInterface;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ChangePwdActivity extends BaseTeacherActivity {


    @InjectView(R.id.editTextOldPwd)
    EditText editTextOldPwd;

    @InjectView(R.id.editTextNewPwd)
    EditText editTextNewPwd;

    @InjectView(R.id.editTextNewPwdRepeat)
    EditText editTextNewPwdRepeat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pwd);
        App.getApp().addActivity(this);

        ButterKnife.inject(this);
    }


    @OnClick(R.id.textViewSave)
    public void onSaveClick() {

        String oldpwd = getInput(editTextOldPwd);
        if (StringUtils.isEmpty(oldpwd)) {
            Tool.showMessageDialog("Old password is empty!", this);
            return;
        }


        String newpwd = getInput(editTextNewPwd);
        if (StringUtils.isEmpty(newpwd)) {
            Tool.showMessageDialog("New password is empty!", this);
            return;
        }

        if(StringUtils.computStrlen(newpwd)<6){
            Tool.showMessageDialog("The new password must be at least 6 characters.", this);
            return;
        }

        String newPwdConfirm = getInput(editTextNewPwdRepeat);
        if (!newpwd.equals(newPwdConfirm)) {
            Tool.showMessageDialog("New passwords do not match.", this);
            return;
        }

        changePwd(oldpwd, newPwdConfirm);

    }


    @OnClick(R.id.viewBack)
    public void onCLickback() {
        finish();
    }


    private void changePwd(final String oldPwd, final String newPwd) {
        reSetTask();

        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jso = new JSONObject();


                    jso.put("old_password", oldPwd);
                    jso.put("new_password", newPwd);

                    netResult = NetInterface.chagnePasswd(ChangePwdActivity.this, jso);

                } catch (Exception e) {

                }


                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null == result) {
                    Tool.showMessageDialog(R.string.error_net, ChangePwdActivity.this);
                } else {
                    if (result.isOk()) {
                       SharePersistent.setObjectValue(ChangePwdActivity.this, KEY_EMAIL_PWD,newPwd);

                        Tool.showMessageDialog("Password has been changed successfully.", ChangePwdActivity.this, new DialogCallBackListener() {
                            @Override
                            public void onDone(boolean yesOrNo) {
                                setResult(RESULT_OK);
                                finish();
                            }
                        });

                    } else {
                        Tool.showMessageDialog(result.getMessage(), ChangePwdActivity.this);
                    }

                }

            }
        });

        baseTask.execute(new HashMap<String, String>());

    }


}
