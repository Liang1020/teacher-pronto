package com.teacherpronto.android.view;

import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.ios.widget.SwitchButton;
import com.range.seekbar.RangeSeekBar;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;
import com.teacherpronto.android.activity.ChangePwdActivity;
import com.teacherpronto.android.activity.MainActivity;
import com.teacherpronto.android.net.NetInterface;

import java.util.HashMap;

/**
 * Created by clia5292 on 2016/4/25.
 */
public class SettingPage extends BasePage {
    private static final String TAG = "SETTINGPAGE";

    private SwitchButton switchPush;
    private SwitchButton switchEmail;
    private RangeSeekBar rangeDistance;

    private Animation animationFadeIn;
    private Animation animationFadeOut;

    private String push;
    private String email;
    private String distance;


    public SettingPage(final MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.rootView = View.inflate(mainActivity, R.layout.page_setting, null);

        animationFadeIn = AnimationUtils.loadAnimation(mainActivity, R.anim.abc_fade_in);
        animationFadeOut = AnimationUtils.loadAnimation(mainActivity, R.anim.abc_fade_out);

        animationFadeIn.setDuration(200);
        animationFadeOut.setDuration(200);

        initView();

        switchPush.setOnStateChangeListener(new SwitchButton.OnStateChangeListener() {
            @Override
            public void onStateChanged(boolean isOn) {
                push = isOn?"1":"0";
                updateSetting(push, email, distance);
            }
        });

        switchEmail.setOnStateChangeListener(new SwitchButton.OnStateChangeListener() {

            @Override
            public void onStateChanged(boolean isOn) {
                email = isOn?"1":"0";
                updateSetting(push, email, distance);
            }
        });

        rangeDistance.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Number minValue, Number maxValue) {
                distance = maxValue.toString();
                updateSetting(push, email, maxValue.toString());
            }
        });


        findViewById(R.id.viewChangePwd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mainActivity, ChangePwdActivity.class);
                mainActivity.startActivity(intent);

            }
        });

    }

    private void initView() {
        switchPush = (SwitchButton) findViewById(R.id.switchButtonPush);
        switchEmail = (SwitchButton) findViewById(R.id.switchButtonEmail);
        rangeDistance = (RangeSeekBar) findViewById(R.id.rangeSeekBarTextColorWithCode);

        push = App.getApp().getUser().getPush_notification();
        email = App.getApp().getUser().getEmail_notification();
        distance = App.getApp().getUser().getDistance_preference();

        if ("0".equals(push)) {
            switchPush.turnOff();
        } else if ("1".equals(push)) {
            switchPush.turnOn();
        }

        if ("0".equals(email)) {
            switchEmail.turnOff();
        } else if ("1".equals(email)) {
            switchEmail.turnOn();
        }

        rangeDistance.setSelectedMaxValue(Integer.valueOf(distance));
    }

    protected void updateSetting(final String push, final String email, final String distance) {
        mainActivity.reSetTask();
        mainActivity.baseTask = new BaseTask(new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    netResult = NetInterface.updateSetting(mainActivity, NetInterface.buildCustomizedObject(paramMap));
                } catch (Exception e) {
                    LogUtil.e(TAG, "error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != result) {


                    if (result.isValidateError()) {

//                        {status": 401, "message": "Unauthorized"}    token认证失败
//                        {"status": 401, message": "invalid_credentials"}   login用户名密码认证失败
//                        {"status": 422, "message": "validation_failed", "error": "{validation failed message here}"}

                        App.logout2Login(mainActivity, result.getCode() + "," + result.getMessage());
                        return;


                    } else
                    if (result.isOk()) {
                        //Tool.showMessageDialog("Setting has been saved!", mainActivity);
                    } else {
                        Tool.showMessageDialog(result.getMessage(), mainActivity);
                    }
                } else {
                    Tool.showMessageDialog("Net error", mainActivity);
                }


            }
        }, mainActivity);

        HashMap<String, String> hmap = new HashMap<String, String>();

        hmap.put("push_notification", push);
        hmap.put("email_notification", email);
        hmap.put("distance_preference", distance);
        hmap.put("api_token", App.getApp().getToken());

        mainActivity.baseTask.execute(hmap);
    }
}
