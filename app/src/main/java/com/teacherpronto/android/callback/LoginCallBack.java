package com.teacherpronto.android.callback;

import com.common.net.NetResult;
import com.teacherpronto.android.bean.User;

/**
 * Created by wangzy on 16/7/15.
 */
public interface LoginCallBack {


    public void onLoginSuccess(User user);
    public void onLoginFail(NetResult netResult);



}
