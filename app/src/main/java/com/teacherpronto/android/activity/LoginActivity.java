package com.teacherpronto.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.Toast;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.util.ValidateTool;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;
import com.teacherpronto.android.bean.User;
import com.teacherpronto.android.callback.LoginCallBack;
import com.teacherpronto.android.dialog.ForgetPasswordDialog;
import com.teacherpronto.android.net.NetInterface;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class LoginActivity extends BaseTeacherActivity {

    @InjectView(R.id.edit_login_email)
    EditText editTextEmail;

    @InjectView(R.id.edit_login_pwd)
    EditText editTextPwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);
        App.getApp().addActivity(this);


        if (App.getApp().hasTempKey("autoLogin") && (boolean) App.getApp().getTempObject("autoLogin") == true) {
            Tool.startActivity(LoginActivity.this, MainActivity.class);
            return;
        }


        String lastEmail = (String) SharePersistent.getObjectValue(this, App.KEY_EMAIL);
        String lastPwd = (String) SharePersistent.getObjectValue(this, KEY_EMAIL_PWD);

        if (!StringUtils.isEmpty(lastEmail)) {
            editTextEmail.setText(lastEmail);
        }

        if (!StringUtils.isEmpty(lastPwd)) {
            editTextPwd.setText(lastPwd);
        }

    }


    @OnClick(R.id.textViewForgetPwd)
    public void OnClickForgetPwd() {

        ForgetPasswordDialog forgetPasswordDialog = new ForgetPasswordDialog(this);

        forgetPasswordDialog.setOnConfirmDialogListener(new ForgetPasswordDialog.OnConfirmDialogListener() {


            @Override
            public void OnConfirmClick(String email) {
                if (!StringUtils.isEmpty(email)) {
                    forgetPwd(email);
                }
            }

            @Override
            public void OncancelClick() {

            }
        });
        forgetPasswordDialog.show();


//        DialogUtils.showInputDialog(this, "Please input your email.", "Confirm", "Cancel", "Please input email.", new DialogCallBackListener() {
//            @Override
//            public void onCallBack(boolean yesNo, String text, DialogInterface dialog) {
//                if (yesNo) {
//                    forgetPwd(text);
//                }
//            }
//        });

    }


    private boolean checkValue() {

        String userName = getInput(editTextEmail);
        if (!ValidateTool.checkEmail(userName)) {
            Tool.showMessageDialog("Email format error!", this);
            return false;
        }

        String pwd = getInput(editTextPwd);
        if (StringUtils.isEmpty(pwd)) {
            Tool.showMessageDialog("Password is empty!", this);
            return false;
        }

        return true;
    }

    private void requestLogin(final String email, final String pwd) {

        reSetTask();

        App.getApp().setToken("");


        requestLogin(this, email, pwd, new LoginCallBack() {
            @Override
            public void onLoginSuccess(User user) {
                Tool.startActivity(LoginActivity.this, MainActivity.class);
            }

            @Override
            public void onLoginFail(NetResult netResult) {

                if (null != netResult) {
                    Tool.showMessageDialog(netResult.getMessage(), LoginActivity.this);
                } else {
                    Tool.showMessageDialog(R.string.error_net, LoginActivity.this);
                }

            }
        });

//        baseTask = new BaseTask(new NetCallBack() {
//
//            @Override
//            public void onPreCall(BaseTask baseTask) {
//                baseTask.showDialogForSelf(true);
//            }
//
//            @Override
//            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
//
//                NetResult netResult = null;
//                try {
//                    netResult = NetInterface.login(LoginActivity.this, NetInterface.buildCustomizedObject(paramMap));
//                } catch (Exception e) {
//                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
//                }
//
//                return netResult;
//            }
//
//            @Override
//            public void onFinish(NetResult result, BaseTask baseTask) {
//                baseTask.hideDialogSelf();
//                if (!isFinishing()) {
//                    if (null != result) {
//                        if (result.isOk()) {
//
//                            User user = (User) result.getData()[0];
//
//                            App.getApp().setUser(user);
//                            OneSignal.sendTag("userid", user.getUser_id());
//                            App.getApp().setIslgout(false);
//                            Tool.startActivity(LoginActivity.this, CropMainActivity.class);
//
//
//                        } else {
//                            Tool.showMessageDialog(result.getMessage(), LoginActivity.this);
//                        }
//                    } else {
//                        Tool.showMessageDialog("Net error", LoginActivity.this);
//                    }
//                }
//            }
//        }, this);
//
//        HashMap<String, String> hmap = new HashMap<String, String>();
//
//        hmap.put("email", email);
//        hmap.put("password", pwd);
//
//        baseTask.execute(hmap);

    }


    public void forgetPwd(final String email) {

        reSetTask();

        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("email", email);
                    netResult = NetInterface.forgetPwd(LoginActivity.this, jsonObject);
                } catch (Exception e) {
                    LogUtil.e(App.tag, "forget pwd error." + e.getLocalizedMessage());

                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
//
//                    if(result.isOk()){
//
//                    }

                    Tool.showMessageDialog(result.getMessage(), LoginActivity.this);
                } else {
                    Tool.showMessageDialog(R.string.error_net, LoginActivity.this);
                }

            }
        });

        baseTask.execute(new HashMap<String, String>());


    }


    @OnClick(R.id.textViewCreateNewAccount)
    public void onClickCreateNew() {

        Tool.startActivity(this, RegistActivity.class);

    }

    @OnClick(R.id.buttonSignIn)
    public void onClickSignIn() {
        if (checkValue()) {

            String email = getInput(editTextEmail);
            String pwd = getInput(editTextPwd);

            requestLogin(email, pwd);

            SharePersistent.setObjectValue(this, App.KEY_EMAIL, email);
            SharePersistent.setObjectValue(this, KEY_EMAIL_PWD, pwd);

        }

        //Tool.startActivity(this, CropMainActivity.class);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String msg = "";

        if (null != intent && !StringUtils.isEmpty(msg = intent.getStringExtra("msg"))) {
            String[] results = msg.split(",");

            Tool.showMessageDialog(results[1], this);

            if ("401".equals(results[0])) {//deactive 竟然会认证失败???

            } else if ("422".equals(results[1])) {//validation failed message here

            }
        }


    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitBy2Click();
        }
        return false;
    }

    private static Boolean isExit = false;

    private void exitBy2Click() {
        Timer tExit = null;
        if (isExit == false) {
            isExit = true; // 准备退出
            Toast.makeText(this, "Click again to exit app!", Toast.LENGTH_SHORT).show();
            tExit = new Timer();
            tExit.schedule(new TimerTask() {
                @Override
                public void run() {
                    isExit = false; // 取消退出
                }
            }, 2000); //
        } else {
            App.getApp().exit();
        }
    }
}
