package com.teacherpronto.android.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.common.callback.NetCallBackMini;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.squareup.picasso.Picasso;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;
import com.teacherpronto.android.bean.Job;
import com.teacherpronto.android.dialog.ConfirmDialog;
import com.teacherpronto.android.net.NetInterface;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class JobDetailActivity extends BaseTeacherActivity {


    @InjectView(R.id.linearLayoutBottomOperate)
    LinearLayout linearLayoutBottomOperate;

    @InjectView(R.id.imaveViewDotStatus)
    ImageView imaveViewDotStatus;

    @InjectView(R.id.textViewStatus)
    TextView textViewStatus;

    @InjectView(R.id.textViewSchoolName)
    TextView textViewSchoolName;

    @InjectView(R.id.textViewAddress)
    TextView textViewAddress;

    @InjectView(R.id.textViewTeacherType)
    TextView textViewTeacherType;

    @InjectView(R.id.textViewSchoolNameDetail)
    TextView textViewSchoolNameDetail;

    @InjectView(R.id.textViewDescJob)
    TextView textViewDescJob;

    @InjectView(R.id.imageViewLogo)
    ImageView imageViewLogo;

    @InjectView(R.id.textViewTeacherLevel)
    TextView textViewTeacherLevel;

    @InjectView(R.id.textViewDurationTime)
    TextView textViewDurationTime;


    @InjectView(R.id.textViewJobTitle)
    TextView textViewJobTitle;

    @InjectView(R.id.textViewJobDate)
    TextView textViewJobDate;

    @InjectView(R.id.textViewJobTime)
    TextView textViewJobTime;


    private Job job;
    private boolean needBackUpdate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail);
        ButterKnife.inject(this);
        job = (Job) App.getApp().getTempObject("job");

        initView();

    }

    @Override
    public void initView() {

        textViewSchoolName.setText(job.getSchool().getSchoolName());
        textViewSchoolNameDetail.setText(job.getSchool().getSchoolName());

        String type = job.getType();
        if (Job.STATUS_ACCEPT.equals(type)) {
            imaveViewDotStatus.setImageResource(R.drawable.icon_dot_green);
            textViewStatus.setText("Accepted");
            linearLayoutBottomOperate.setVisibility(View.GONE);
        }
        if (job.STATUS_DECLINE.equals(type)) {
            imaveViewDotStatus.setImageResource(R.drawable.icon_dot_red);
            textViewStatus.setText("Declined");
            linearLayoutBottomOperate.setVisibility(View.GONE);
        }
        if (job.STATUS_NEW.equals(type)) {
            imaveViewDotStatus.setImageResource(R.drawable.icon_dot_blue);
            textViewStatus.setText("New Job");

        }

        if (job.STATUS_CANCELED.equals(type)) {
            imaveViewDotStatus.setImageResource(R.drawable.icon_dot_canceled);
            textViewStatus.setText("Canceled Job");
            linearLayoutBottomOperate.setVisibility(View.GONE);

        }



        textViewJobTitle.setText(job.getJobTitle());

        textViewJobDate.setText(job.getStartDateText() + "-" + job.getEndDateText());

        textViewJobTime.setText(job.getStartTime().replace("am", " AM").replace("pm", " PM") + "-" + job.getEndTime().replace("am", " AM").replace("pm", " PM"));


//        String durationTime = job.getStartDate() + " " + job.getStartTime() + " - " + job.getEndDate() + " " + job.getEndTime();
//
//        durationTime = durationTime.replace("am", " AM");
//        durationTime = durationTime.replace("pm", " PM");
//
//
//        textViewDurationTime.setText(durationTime);


        textViewAddress.setText((job.getSchool().getAddressTreet() + " " + job.getSchool().getAddressSuburb() + " " + job.getSchool().getAddressState() + " " + job.getSchool().getAddressPostCode()).trim());

        textViewDescJob.setText(job.getJobDescription());

        textViewTeacherType.setText(job.getTeacherTypeText());


        ArrayList<String> levelexpers = job.getTeachingLevels();

        if (!ListUtiles.isEmpty(levelexpers)) {

            if (ListUtiles.getListSize(levelexpers) == 1) {
                textViewTeacherLevel.setText(levelexpers.get(0));


//                if (levelexpers.get(0).equals("15")) {
//                    textViewTeacherLevel.setText("Universify Graduate");
//                } else if (levelexpers.get(0).equals("1")) {
//                    textViewTeacherLevel.setText("Yr.Prep/Kindergarten,Yr.1,Yr.2,Yr.3,Yr.4,Yr.5,Yr.6");
//                } else {
////                    textViewTeacherLevel.setText("Yr." + (Integer.parseInt(levelexpers.get(0)) - 2));
//                    textViewTeacherLevel.setText(levelexpers.get(0));
//                }

            } else {


                StringBuffer levelbuf = new StringBuffer();
                for (int i = 0, isize = levelexpers.size(); i < isize; i++) {


                    String level = "";
                    try {
                        level = String.valueOf(Integer.parseInt(levelexpers.get(i)) - 2);

                        if (i != (isize - 1)) {
                            levelbuf.append("Yr." + level + ",");
                        } else {
                            levelbuf.append("Yr." + level);
                        }

                    } catch (Exception e) {
                        level = levelexpers.get(i);


                        if (i != (isize - 1)) {
                            levelbuf.append(level + ",");
                        } else {
                            levelbuf.append(level);
                        }
                    }


                }


                textViewTeacherLevel.setText(levelbuf);
            }

        }


        if (null != job.getSchool() && (!StringUtils.isEmpty(job.getSchool().getLog()))) {
            Picasso.with(App.getApp()).load(job.getSchool().getLog()).into(imageViewLogo);
        }
//        imageViewLogo
    }

    @OnClick(R.id.buttonAccept)
    public void onClickAccept() {

        ConfirmDialog confirmAcceptJobDialog = new ConfirmDialog(this,"Job Accept", "Are you sure you want to accept this job?");

        confirmAcceptJobDialog.setOnConfirmDialogListener(new ConfirmDialog.OnConfirmDialogListener() {
            @Override
            public void OnConfirmClick() {


                aceptOrDelayJob(job, true);
            }

            @Override
            public void OncancelClick() {

            }
        });

        confirmAcceptJobDialog.show();
    }

    @OnClick(R.id.buttonDecline)
    public void onClickDecline() {


        ConfirmDialog confirmAcceptJobDialog = new ConfirmDialog(this, "Job Decline", "Are you sure you want to decline this job?");

        confirmAcceptJobDialog.setOnConfirmDialogListener(new ConfirmDialog.OnConfirmDialogListener() {
            @Override
            public void OnConfirmClick() {


                aceptOrDelayJob(job, false);
            }

            @Override
            public void OncancelClick() {

            }
        });

        confirmAcceptJobDialog.show();

    }


    @OnClick(R.id.viewBack)
    public void onClickback() {

        backResult(needBackUpdate);
    }


    public void acceptDelayJob(final Context context, final boolean showDialog, final String jobId, final boolean acptDey, final NetCallBackMini netCallBack) {

        reSetTask();

        baseTask = new BaseTask(this, new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    baseTask.showDialogForSelf(true);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;

                try {

                    JSONObject requestObj = new JSONObject();
                    requestObj.put("job_id", jobId);
                    requestObj.put("deal", acptDey ? "1" : "0");

                    netResult = NetInterface.acetDenyJob(context, requestObj);
                } catch (Exception e) {
                }


                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (showDialog) {
                    baseTask.hideDialogSelf();
                }

                if (null != netCallBack) {
                    netCallBack.onFinish(result);
                }


            }
        });
        baseTask.execute(new HashMap<String, String>());

    }


    public void aceptOrDelayJob(final Job job, final boolean isAccept) {

        acceptDelayJob(this, true, job.getJobId(), isAccept, new NetCallBackMini() {
            @Override
            public void onFinish(NetResult netResult) {

                if (null != netResult) {

                    if (netResult.isOk()) {
                        needBackUpdate = true;

                        Tool.showMessageDialog(netResult.getMessage(), JobDetailActivity.this);

                        if (isAccept) {
                            job.setType(Job.STATUS_ACCEPT);
                        } else {
                            job.setType(Job.STATUS_DECLINE);
                        }

                        initView();

                    } else {
                        Tool.showMessageDialog(netResult.getMessage(), JobDetailActivity.this);
                    }

                } else {
                    Tool.showMessageDialog(R.string.error_net, JobDetailActivity.this);
                }

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        backResult(needBackUpdate);
    }

    public void backResult(boolean needBackUpdate) {

        if (needBackUpdate) {
            App.getApp().putTemPObject("job_status", job.getType());
            setResult(RESULT_OK);
        }

        finish();
    }
}
