package com.teacherpronto.android.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.callback.OnUploadCallBack;
import com.common.BaseActivity;
import com.common.net.FormFile;
import com.common.net.NetException;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.BitmapTool;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.onesignal.OneSignal;
import com.teacherpronto.android.App;
import com.teacherpronto.android.bean.User;
import com.teacherpronto.android.callback.LoginCallBack;
import com.teacherpronto.android.net.NetInterface;
import com.teacherpronto.android.util.SharedpreferencesUtil;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;

/**
 * Created by wangzy on 16/4/12.
 */
public class BaseTeacherActivity extends BaseActivity {

    public static final String KEY_EMAIL_PWD = "email_pwd";

    public final short SHORT_REQUEST_ADDCATCH = 1000;
    public final short SHORT_REQUEST_SELECTIMAGE = 1001;
    public final short SHORT_ACCESS_LOCATION = 1003;
    public final short SHORT_REQUEST_READ_EXTEAL = 1004;
    public final short SHORT_REQUEST_READ_EXTEAL_CATCH = 1005;
    public final short SHORT_REQUEST_MEDIAS = 1006;
    public final short SHORT_REQUEST_PHONE_STATE = 1007;
    public final short SHORT_REQUEST_CANCEL_VERIFY = 1008;
    public final short SHORT_REQUESET_VERIFY = 1009;

    public short SELECT_PIC_KITKAT = 801;
    public short SELECT_PIC = 802;
    public short TAKE_BIG_PICTURE = 803;
    public short CROP_BIG_PICTURE = 804;
    public int outputX = 512;
    public int outputY = 512;

    public byte TYPE_REQEST_PICTURE_TAKE = 0;
    public byte TYPE_REQEST_PICTURE_SELECT = 1;

    public byte type_request_picture = TYPE_REQEST_PICTURE_TAKE;

    public SharedpreferencesUtil sharedpreferencesUtil = new SharedpreferencesUtil(this);

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

    }

    public void startTakePicture() {
        type_request_picture = TYPE_REQEST_PICTURE_TAKE;
        if (!sharedpreferencesUtil.getImageTempNameUri().equals("")) {
            sharedpreferencesUtil.delectImageTemp();
        }
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, sharedpreferencesUtil.getImageTempNameUri());
        startActivityForResult(intent, TAKE_BIG_PICTURE);
    }

    public void startSelectPicture() {
        type_request_picture = TYPE_REQEST_PICTURE_SELECT;
        if (!sharedpreferencesUtil.getImageTempNameUri().equals("")) {
            sharedpreferencesUtil.delectImageTemp();
        }
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/jpeg");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            startActivityForResult(intent, SELECT_PIC_KITKAT);
        } else {
            startActivityForResult(intent, SELECT_PIC);
        }
    }


    public void onTakePhtoClick() {

        type_request_picture = TYPE_REQEST_PICTURE_TAKE;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                    ) {
                startTakePicture();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, SHORT_REQUEST_READ_EXTEAL);
            }
        } else {
            startTakePicture();
        }
    }

    public void onChosePhtoClick() {
        type_request_picture = TYPE_REQEST_PICTURE_SELECT;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                startSelectPicture();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission_group.CAMERA}, SHORT_REQUEST_READ_EXTEAL);
            }
        } else {
            startSelectPicture();
        }
    }

    public void cropImageUri(Uri uriIn, Uri uriOut, int outputX, int outputY, int requestCode) {


        //======use local api

//        Intent intent = new Intent("com.android.camera.action.CROP");
//        intent.setDataAndType(uriIn, "image/*");
//        intent.putExtra("crop", "true");
//        intent.putExtra("aspectX", 1);
//        intent.putExtra("aspectY", 1);
//        intent.putExtra("outputX", outputX);
//        intent.putExtra("outputY", outputY);
//        intent.putExtra("scale", true);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriOut);
//        intent.putExtra("return-data", false);
//        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
//        intent.putExtra("noFaceDetection", true); // no face detection
//        startActivityForResult(intent, requestCode);


        //===============use crop new api=========
        String imgUrl = uriIn.getPath();

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imgUrl, options);

        int w = options.outWidth;
        int h = options.outHeight;

        if (w < outputX || h < outputY) {
            Tool.showMessageDialog("This picture is too small please select biger picture", this);
            return;
        }

        CropImage.activity(uriIn)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this, outputX, outputY, outputX, outputY, CropImageView.CropShape.RECTANGLE, Bitmap.CompressFormat.PNG);


    }


//    private void startCropImageActivity(Uri imageUri, int width, int height) {
//        CropImage.activity(imageUri)
//                .setGuidelines(CropImageView.Guidelines.ON)
//                .start(this);
//    }


    protected void upLoadHeaderImg(final Context context, final String fileUrl, final OnUploadCallBack onUploadCallBack) {
        reSetTask();
        baseTask = new BaseTask(new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    byte[] b = BitmapTool.readStream(new FileInputStream(fileUrl));

                    LogUtil.i(App.tag, "upload file len:" + b.length);

                    FormFile form = new FormFile("avatar", b, "crop_cache_file.png", "application/octet-stream");
                    FormFile[] temp = {form};
                    HashMap<String, String> map = new HashMap<String, String>();

                    LogUtil.i(App.tag, "token:" + App.getApp().getToken());
                    map.put("api_token", App.getApp().getToken());
                    netResult = NetInterface.uploadAvatar(temp, map);
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.tag, "upload error:" + e.getLocalizedMessage());
                    if (e instanceof NetException) {
                        LogUtil.e(App.tag, "upload error:" + ((NetException) e).getCode());
                    }
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != onUploadCallBack) {
                    onUploadCallBack.onUploadEnd(result, baseTask.weakReferenceContext.get());
                }


                if (null != result && StringUtils.isEmpty(result.getMessage())) {
                    try {
                        File file = new File(fileUrl);
                        if (null != file && file.exists() && file.isFile()) {
                            file.delete();
                        }
                        LogUtil.e(App.tag, "deleted file:" + fileUrl);
                    } catch (Exception e) {
                        LogUtil.e(App.tag, "delete file fail");
                    }

                }


            }
        }, context);
        baseTask.execute(new HashMap<String, String>());
    }

    protected void sendVerification(final Context context) {
        reSetTask();
        baseTask = new BaseTask(new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("api_token", App.getApp().getToken());
                    LogUtil.i(App.tag, "token:" + App.getApp().getToken());
                    netResult = NetInterface.getVerify(context, jsonObject);

                } catch (Exception e) {
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != result) {
                    if (result.isOk()) {
                        Tool.showMessageDialog("Verification code has been sent to your email!", (Activity) context);

                    } else {
                        Tool.showMessageDialog(result.getMessage(), (Activity) context);
                    }
                } else {
                    Tool.showMessageDialog("Net error", (Activity) context);
                }


            }
        }, context);
        baseTask.execute(new HashMap<String, String>());
    }


    public static void sendVerificationQuality(final Context context) {

        BaseTask sendVeryTask = new BaseTask(context, new NetCallBack() {
            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("api_token", App.getApp().getToken());
                    LogUtil.i(App.tag, "token:" + App.getApp().getToken());
                    netResult = NetInterface.getVerify(context, jsonObject);

                } catch (Exception e) {
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (null != result && result.isOk()) {

                    LogUtil.e(App.tag, "send Verify quality success");
                } else {
                    LogUtil.e(App.tag, "send Verify quality fail");
                }
            }
        });


        sendVeryTask.execute(new HashMap<String, String>());
    }


    protected void verify(final Context context, final String verificationCode) {
        reSetTask();
        baseTask = new BaseTask(new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {

                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = null;
                try {
                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("api_token", App.getApp().getToken());
                    jsonObject.put("verify_code", verificationCode);

                    LogUtil.i(App.tag, "token:" + App.getApp().getToken());
                    netResult = NetInterface.verify(context, jsonObject);

                } catch (Exception e) {
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();

                if (null != result) {
                    if (result.isOk()) {
                        if (result.getMessage().equals("verify success") || result.getMessage().equals("already_verified")) {
                            Intent intent = new Intent();
                            ((Activity) context).setResult(RESULT_OK, intent);
                            ((Activity) context).finish();
                        } else {
                            Tool.startActivity(context, EditProfileActivity.class);
                        }

                    } else {
                        Tool.showMessageDialog(result.getMessage(), (Activity) context);
                    }
                } else {
                    Tool.showMessageDialog("Net error", (Activity) context);
                }


            }
        }, context);
        baseTask.execute(new HashMap<String, String>());
    }


    public void requestLogin(final Activity activity, final String email, final String pwd, final LoginCallBack loginCallBack) {

        reSetTask();

        App.getApp().setToken("");

        baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.login(activity, NetInterface.buildCustomizedObject(paramMap));
                } catch (Exception e) {
                    LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (!isFinishing()) {
                    if (null != result) {
                        if (result.isOk()) {

                            User user = (User) result.getData()[0];

                            App.getApp().setUser(user);
                            OneSignal.sendTag("userid", user.getUser_id());
                            App.getApp().setIslgout(false);
                            if (null != loginCallBack) {
                                loginCallBack.onLoginSuccess(user);
                            }

                        } else {


                            if (null != loginCallBack) {
                                loginCallBack.onLoginFail(result);
                            }
                        }
                    } else {
                        if (null != loginCallBack) {
                            loginCallBack.onLoginFail(result);
                        }
                    }
                }
            }
        }, this);

        HashMap<String, String> hmap = new HashMap<String, String>();

        hmap.put("email", email);
        hmap.put("password", pwd);

        baseTask.execute(hmap);

    }


    @Override
    protected void onResume() {
        super.onResume();
        App.getApp().activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.getApp().activityPaused();
    }
}
