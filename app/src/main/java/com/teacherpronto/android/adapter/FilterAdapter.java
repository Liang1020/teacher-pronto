package com.teacherpronto.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.teacherpronto.android.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 16/5/24.
 */
public class FilterAdapter extends BaseAdapter {


    private String[] values;
    private String[] labels;
    private Context context;
    private LayoutInflater layoutInflater;
    private int index = -1;


    public FilterAdapter(Context context, String[] values, String[] labels) {
        this.context = context;
        this.values = values;
        this.labels = labels;
        this.layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {

        return labels.length;
    }

    @Override
    public Object getItem(int position) {
        return values[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_filter, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textViewFilter.setText(labels[position]);
        viewHolder.textViewFilter.setTag(values[position]);

        if (index == position) {
            viewHolder.imageViewHook.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imageViewHook.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    class ViewHolder {


        @InjectView(R.id.textViewFilter)
        TextView textViewFilter;

        @InjectView(R.id.imageViewHook)
        ImageView imageViewHook;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }


    public void setIndex(int index) {
        this.index = index;
        notifyDataSetChanged();
    }
}
