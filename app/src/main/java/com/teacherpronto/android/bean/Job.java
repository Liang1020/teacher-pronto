package com.teacherpronto.android.bean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by wangzy on 16/5/3.
 */
public class Job {

    private String jobId;
    private String type;
    private String typeDescription;
    private String startDate;
    private String endDate;
    private String startTime;
    private String endTime;
    private String teacherType;

    private ArrayList<String> teachingLevels;
    private String jobTitle;
    private String jobDescription;
    private String canced;
    private String expired;
    private School school;

    private long offer_end_time;
    private int time_out_second;


    public static final String STATUS_ACCEPT = "2";
    public static final String STATUS_NEW = "1";
    public static final String STATUS_DECLINE = "3";
    public static final String STATUS_CANCELED = "4";


    public Job() {
        teachingLevels = new ArrayList<>();
    }

    public void addTeahingLeve(String level) {

        teachingLevels.add(level);
    }

    public ArrayList<String> getTeachingLevels() {

        return teachingLevels;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeDescription() {
        return typeDescription;
    }

    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTeacherType() {
        return teacherType;
    }

    public void setTeacherType(String teacherType) {
        this.teacherType = teacherType;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }


    public String getCanced() {
        return canced;
    }

    public void setCanced(String canced) {
        this.canced = canced;
    }

    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public long getOffer_end_time() {
        return offer_end_time;
    }

    public void setOffer_end_time(long offer_end_time) {
        this.offer_end_time = offer_end_time;
    }

    public String getTeacherTypeText() {

        if ("1".equals(teacherType)) {
            return "Primary";
        } else if ("2".equals(teacherType)) {
            return "Secondary";
        } else if ("3".equals(teacherType)) {
            return "P-12";
        } else if ("4".equals(teacherType)) {
            return "Other";
        }

        return "";
    }


    SimpleDateFormat sdfAu = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sdfAuParse = new SimpleDateFormat("yyyy-MM-dd");

    public String getStartDateText() {


        try {

            return sdfAu.format(sdfAuParse.parse(getStartDate()));
        } catch (Exception e) {

        }

        return startDate;
    }

    public String getEndDateText() {
        try {

            return sdfAu.format(sdfAuParse.parse(getEndDate()));
        } catch (Exception e) {

        }

        return endDate;
    }


    public int getTime_out_second() {
        return time_out_second;
    }

    public void setTime_out_second(int time_out_second) {
        this.time_out_second = time_out_second;
    }
}
