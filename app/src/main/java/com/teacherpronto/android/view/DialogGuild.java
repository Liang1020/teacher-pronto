package com.teacherpronto.android.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.common.util.SharePersistent;
import com.teacherpronto.android.R;

/**
 * Created by wangzy on 16/9/18.
 */
public class DialogGuild extends MyBasePicker {

    public DialogGuild(final Context context) {
        super(context);
        this.rootView = (ViewGroup) View.inflate(context, R.layout.dialog_guild_avaliable, null);


        this.rootView.findViewById(R.id.viewCover).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharePersistent.saveBoolean(context, "see_guild_ava", true);
                dismiss();
            }
        });

    }
}
