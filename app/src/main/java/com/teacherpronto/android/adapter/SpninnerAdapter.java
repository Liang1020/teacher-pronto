package com.teacherpronto.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.common.util.LogUtil;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 16/5/24.
 */
public class SpninnerAdapter extends BaseAdapter {


    private String[] values;
    private String[] labels;
    private Context context;
    private LayoutInflater layoutInflater;

    private int count = 0;

    public SpninnerAdapter(Context context, String[] values, String[] labels) {
        this.context = context;
        this.values = values;
        this.labels = labels;
        this.layoutInflater = LayoutInflater.from(context);
        this.count = values.length;
    }


    @Override
    public int getCount() {

        return count = labels.length;
    }

    @Override
    public Object getItem(int position) {
        return values[position % count];
    }

    @Override
    public long getItemId(int position) {
        return position % count;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_spinner, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        try {
            viewHolder.textViewOptions.setText(labels[position % count]);
            viewHolder.textViewOptions.setTag(values[position % count]);
        } catch (Exception e) {

            LogUtil.i(App.tag, "build adapter faillter");
        }


        return convertView;
    }

    class ViewHolder {


        @InjectView(R.id.textViewOptions)
        TextView textViewOptions;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }
}
