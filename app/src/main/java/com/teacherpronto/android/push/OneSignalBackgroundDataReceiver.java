package com.teacherpronto.android.push;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;

import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;
import com.teacherpronto.android.activity.MainActivity;
import com.teacherpronto.android.activity.SplashActivity;
import com.teacherpronto.android.view.NotifyDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

/*
Example JSON payload.
{
  "app_id" : "YOUR_ONESIGNAL_APP_ID",
  "include_player_ids": ["YOUR_ONESIGNAL_PLAYER_ID"],
  "contents" : {
"en" : "Message 1"
},
  "android_background_data": true,
  "data": {"yourCustomKey": "value123"}
}
*/

public class OneSignalBackgroundDataReceiver extends BroadcastReceiver {

    public void onReceive(Context context, Intent intent) {
        Bundle dataBundle = intent.getBundleExtra("data");


        LogUtil.e(App.tag, "push receive in from onesignal.....onReceive");


        try {
            Log.e("OneSignalExample", "NotificationTable title: " + dataBundle.getString("title"));
            Log.e("OneSignalExample", "data additionalData: " + dataBundle.getString("custom"));
            JSONObject customJSON = new JSONObject(dataBundle.getString("custom"));
            if (customJSON.has("a")) {

                JSONObject additionalData = customJSON.getJSONObject("a");
                if (additionalData.has("alert")) {

                    notification(context, additionalData);

                }
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }


    private void notification(Context context, JSONObject pushData) {

//      if (App.getApp().getFlagLogout() || null == App.getApp().getUser()) {
//         return;
//      }


        LogUtil.e(App.tag, "push receive in from onesignal.....notification");

        if (pushData != null && (pushData.has("alert") || pushData.has("title"))) {

            String title = pushData.optString("title", context.getResources().getString(R.string.app_name));
            String alert = pushData.optString("alert", "Notification received.");
            final int type = pushData.optInt("type", -1);
            String tickerText = String.format(Locale.getDefault(), "%s: %s", new Object[]{title, alert});


            Intent i = null;
            if (App.getApp().isMainCreated() && App.getApp().appVisible) {

                if (null != App.getApp().getMainActivity()) {

//                    (App.getApp().getMainActivity()).onPushReceive(pushData);

                    final List<Activity> activities = App.getApp().getAllActivitys();
                    if (!ListUtiles.isEmpty(activities)) {

                        for (int xsize = activities.size() - 1, x = xsize; x >= 0; x--) {//find a root activity

                            final Activity activity = activities.get(x);
                            if (null != activity && !activity.isFinishing() ) {

                                NotifyDialog notifyDialog = new NotifyDialog(activity, alert);

                                notifyDialog.setOnOKclickListener(new NotifyDialog.OnOKclickListener() {
                                    @Override
                                    public void onOkClick() {

                                        if (type == 1) {//new job,goto main & refresh

                                            if (activity instanceof MainActivity) {
                                                ((MainActivity) activity).showBookingsViewAndRefresh();
                                            } else {
                                                Intent intent = new Intent(activity, MainActivity.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                activity.startActivity(intent);
                                            }

                                        } else if (type == 2) {//cancel job

                                            if (activity instanceof MainActivity) {
                                                ((MainActivity) activity).showBookingsViewAndRefresh();
                                            } else {
                                                Intent intent = new Intent(activity, MainActivity.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                activity.startActivity(intent);
                                            }

                                        }

                                    }
                                });
                                notifyDialog.show();
                                return;
                            }
                        }
                    }


                } else {
                    i = new Intent(context, MainActivity.class);
                }
            } else {
                i = new Intent(context, SplashActivity.class);
            }

            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);


            PendingIntent pi = PendingIntent.getActivity(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

//         if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
//            // 下面需兼容Android 2.x版本是的处理方式
//            // Notification notify1 = new Notification(R.drawable.message,
//            // "TickerText:" + "您有新短消息，请注意查收！", System.currentTimeMillis());
//            Notification notify1 = new Notification();
//            notify1.icon = R.mipmap.ic_launcher;
//            notify1.when = System.currentTimeMillis();
//            notify1.setLatestEventInfo(context, title, alert, pi);
//            notify1.number = 1;
//            notify1.flags |= Notification.FLAG_AUTO_CANCEL; // FLAG_AUTO_CANCEL表明当通知被用户点击时，通知将被清除。
//            nm.notify(R.string.app_name, notify1);
//
//         } else

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB && android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {

//                PendingIntent pendingIntent2 = PendingIntent.getActivity(context, 0,i, 0);
                Notification notify2 = new Notification.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        // 设置状态栏中的小图片，尺寸一般建议在24×24，这个图片同样也是在下拉状态栏中所显示
                        // ，如果在那里需要更换更大的图片，可以使用setLargeIcon(Bitmap
                        // icon)
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                        .setTicker(alert)// 设置在status
                        .setContentTitle(title)// 设置在下拉status
                        // bar后Activity，本例子中的NotififyMessage的TextView中显示的标题
                        .setContentText(alert)// TextView中显示的详细内容
                        .setContentIntent(pi) // 关联PendingIntent
                        .setNumber(1) // 在TextView的右方显示的数字，可放大图片看，在最右侧。这个number同时也起到一个序列号的左右，如果多个触发多个通知（同一ID），可以指定显示哪一个。

                        .getNotification(); // 需要注意build()是在API level
                // 16及之后增加的，在API11中可以使用getNotificatin()来代替
                notify2.flags |= Notification.FLAG_AUTO_CANCEL;
                nm.notify(R.string.app_name, notify2);

            } else if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {

//                PendingIntent pendingIntent3 = PendingIntent.getActivity(context, 0,i, 0);
                // 通过Notification.Builder来创建通知，注意API Level
                // API16之后才支持
                Notification notify3 = null;

                notify3 = new Notification.Builder(context)
                        .setSmallIcon(getNotificationIcon())
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                        .setTicker(alert)
                        .setContentTitle(title)
                        .setContentText(alert)
                        .setContentIntent(pi).setNumber(1).build();
                // 需要注意build()是在API
                // level16及之后增加的，API11可以使用getNotificatin()来替代
                notify3.flags |= Notification.FLAG_AUTO_CANCEL; // FLAG_AUTO_CANCEL表明当通知被用户点击时，通知将被清除。
                nm.notify(R.string.app_name, notify3);// 步骤4：通过通知管理器来发起通知。如果id不同，则每click，在status哪里增加一个提示

            }

//            NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//            Notification n = new Notification(R.mipmap.ic_launcher, title, System.currentTimeMillis());
//            n.flags = Notification.FLAG_AUTO_CANCEL;
//            n.defaults = Notification.DEFAULT_ALL;
//            n.setLatestEventInfo(context, title, alert, pi);
//            nm.notify(R.string.app_name, n);


        }

    }

    private int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return whiteIcon ? R.drawable.icon_tip_push_s : R.mipmap.ic_launcher;

//        return R.mipmap.ic_launcher;
    }


    private JSONObject getPushData(Intent intent) {
        try {
            return new JSONObject(intent.getStringExtra("com.parse.Data"));
        } catch (JSONException var3) {
            LogUtil.e("com.parse.ParsePushReceiver", "Unexpected JSONException when receiving push data: ", var3);
            return null;
        }
    }

}