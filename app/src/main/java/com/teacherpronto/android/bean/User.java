package com.teacherpronto.android.bean;

import com.common.bean.IBeanInterface;

import java.util.ArrayList;

/**
 * Created by clia on 2016/5/23.
 */
public class User implements IBeanInterface {

    private static final long serialVersionUID = 1L;

    private String user_id;
    private String email;
    private String password;

    private String first_name;
    private String last_name;
    private String push_notification;
    private String email_notification;
    private String distance_preference;


    private String form_lodged;
    private String register_type;
    private String register_number;
    private String register_state;
    private String phone;
    private String teaching_preference;


    private String post_code;
    private String latitude;
    private String longitude;
    private String year_experience;
    private String indroduction;
    private String avatar;
    private String verify;
    private String token;



    private ArrayList<String> level_experience;
    private ArrayList<String> subjects_preference;


    public User() {
        level_experience = new ArrayList<>();
        subjects_preference = new ArrayList<>();
    }

    public void addLevelExperences(String levelExperences) {
        level_experience.add(levelExperences);
    }

    public void addSubjectsPreference(String subjectPreference) {
        subjects_preference.add(subjectPreference);
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRegister_number() {
        return register_number;
    }

    public void setRegister_number(String register_number) {
        this.register_number = register_number;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getForm_lodged() {
        return form_lodged;
    }

    public void setForm_lodged(String form_lodged) {
        this.form_lodged = form_lodged;
    }

    public String getRegister_type() {
        return register_type;
    }

    public void setRegister_type(String register_type) {
        this.register_type = register_type;
    }

    public String getRegister_state() {
        return register_state;
    }

    public void setRegister_state(String register_state) {
        this.register_state = register_state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTeaching_preference() {
        return teaching_preference;
    }

    public void setTeaching_preference(String teaching_preference) {
        this.teaching_preference = teaching_preference;
    }

    public String getYear_experience() {
        return year_experience;
    }

    public void setYear_experience(String year_experience) {
        this.year_experience = year_experience;
    }

    public String getIndroduction() {
        return indroduction;
    }

    public void setIndroduction(String indroduction) {
        this.indroduction = indroduction;
    }

    public ArrayList<String> getSubjects_preference() {
        return subjects_preference;
    }

    public void setSubjects_preference(ArrayList<String> subjects_preference) {
        this.subjects_preference = subjects_preference;
    }

    public ArrayList<String> getLevel_experience() {
        return level_experience;
    }

    public void setLevel_experience(ArrayList<String> level_experience) {
        this.level_experience = level_experience;
    }

    public String getPush_notification() {
        return push_notification;
    }

    public void setPush_notification(String push_notification) {
        this.push_notification = push_notification;
    }

    public String getEmail_notification() {
        return email_notification;
    }

    public void setEmail_notification(String email_notification) {
        this.email_notification = email_notification;
    }

    public String getDistance_preference() {
        return distance_preference;
    }

    public void setDistance_preference(String distance_preference) {
        this.distance_preference = distance_preference;
    }

    public String getVerify() {
        return verify;
    }
    public boolean isVerify(){
        return "1".equals(verify)?true:false;
    }

    public void setVerify(String verify) {
        this.verify = verify;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
