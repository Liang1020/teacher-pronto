package com.teacherpronto.android.activity;

import android.os.Bundle;

import com.common.net.NetResult;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;
import com.teacherpronto.android.bean.User;
import com.teacherpronto.android.callback.LoginCallBack;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends BaseTeacherActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        App.getApp().addActivity(this);

        if (App.getApp().isLogout()) {
            delay2go();
        } else {
            String lastEmail = (String) SharePersistent.getObjectValue(App.getApp(), App.KEY_EMAIL);
            String lastPwd = (String) SharePersistent.getObjectValue(App.getApp(), KEY_EMAIL_PWD);

            if (!StringUtils.isEmpty(lastEmail) && !StringUtils.isEmpty(lastPwd)) {


                requestLogin(SplashActivity.this, lastEmail, lastPwd, new LoginCallBack() {
                    @Override
                    public void onLoginSuccess(User user) {
                        App.getApp().putTemPObject("autoLogin", true);
                        Tool.startActivity(SplashActivity.this, LoginActivity.class);
                    }

                    @Override
                    public void onLoginFail(NetResult netResult) {
                        Tool.startActivity(SplashActivity.this, LoginActivity.class);
                    }
                });

            } else {
                delay2go();
            }

        }


    }


    public void delay2go() {

        Timer timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(SharePersistent.getBoolean(SplashActivity.this,"see_guild")){
                    Tool.startActivity(SplashActivity.this, LoginActivity.class);
                }else{
                    Tool.startActivity(SplashActivity.this, GuidActivity.class);
                }

            }
        }, 2 * 1000);
    }

}
