package com.teacherpronto.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bigkoo.pickerview.view.MyBasePicker;
import com.teacherpronto.android.R;

/**
 * Created by wangzy on 16/7/14.
 */
public class NotifyDialog extends MyBasePicker {


    private TextView textViewContent;
    private Button buttonOk;
    private OnOKclickListener onOKclickListener;

    public NotifyDialog(Context context, String content) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_notify, contentContainer);

        textViewContent = (TextView) findViewById(R.id.textViewNotifyContent);

        textViewContent.setText(content);

        buttonOk = (Button) findViewById(R.id.buttonOk);

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();

                if (null != onOKclickListener) {

                    onOKclickListener.onOkClick();
                }
            }
        });
    }


    public static interface OnOKclickListener {

        public void onOkClick();
    }


    public OnOKclickListener getOnOKclickListener() {
        return onOKclickListener;
    }

    public void setOnOKclickListener(OnOKclickListener onOKclickListener) {
        this.onOKclickListener = onOKclickListener;
    }
}
