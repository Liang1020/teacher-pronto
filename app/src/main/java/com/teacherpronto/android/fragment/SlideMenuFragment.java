package com.teacherpronto.android.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.teacherpronto.android.R;
import com.teacherpronto.android.activity.MainActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by clia5292 on 2016/4/20.
 */
public class SlideMenuFragment extends Fragment{
    private MainActivity activity;
    private OnSlideMenuClickListener onSlideMenuClickListener;

    private View rootView;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = (MainActivity) getActivity();
        rootView = View.inflate(getActivity(), R.layout.fragment_slide_menu, null);
        ButterKnife.inject(this, rootView);
        return rootView;
    }

    public OnSlideMenuClickListener getOnSlideMenuClickListener() {
        return onSlideMenuClickListener;
    }

    public void setOnSlideMenuClickListener(OnSlideMenuClickListener onSlideMenuClickListener) {
        this.onSlideMenuClickListener = onSlideMenuClickListener;
    }

    public static interface OnSlideMenuClickListener {

        public void onLogOutClick();
    }

    @OnClick(R.id.linearLayoutBookings)
    protected void onClickBookings() {
        activity.showBookingsView();
        activity.toggle();
    }

    @OnClick(R.id.linearLayoutAvailability)
    public void onClickAvailability() {
        activity.showAvaiablepage();
        activity.toggle();
    }

    @OnClick(R.id.linearLayoutProfile)
    protected void onClickProfile() {
        activity.showProfileView();
        activity.toggle();
    }


    @OnClick(R.id.linearLayoutSettings)
    public void onClickSetting() {
        activity.showSettingView();
        activity.toggle();
    }


    @OnClick(R.id.linearLayoutLogOut)
    public void onWorkShopsClick() {

        if (null != onSlideMenuClickListener) {
            onSlideMenuClickListener.onLogOutClick();
        }
    }
}


