package com.teacherpronto.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Environment;

import com.common.BaseApp;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.onesignal.OneSignal;
import com.teacherpronto.android.activity.LoginActivity;
import com.teacherpronto.android.activity.MainActivity;
import com.teacherpronto.android.bean.User;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

/**
 * Created by wangzy on 16/4/12.
 */
public class App extends BaseApp {

    public static final String tag = "TEACHER_PRONTO";
    public static final String KEY_EMAIL = "Email";
    public static final String DirFileName = "cretve_cache";

    public static final boolean isFree = false;
    public static File CACHE_DIR_IMAGE;
    public static int RESULT_CODE_CAMERA = 1;
    public static String CACHE_DIR;
    public static String CACHE_DIR_PRODUCTS;
    public static String CACHE_DIR_EXCEL;
    public static String CACHE_DIR_PDF;

    private final String KEY_USER = "keyUser";

    private static App app;
    private String token;
    private int verified;
    private User user;
    private HashMap<String, Object> tempParamap;
    private boolean isMaincreate;
    private boolean isMainFront;

    private MainActivity mainActivity;


    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        this.tempParamap = new HashMap<String, Object>();


        initOnesignal();
    }



    public static void logout2Login(Activity context, String message) {

        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra("msg", message);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
        ((Activity) context).finish();
    }


    private void initOnesignal() {

        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.WARN);

        OneSignal.startInit(this)
                .setAutoPromptLocation(true).setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
            @Override
            public void notificationOpened(String message, JSONObject additionalData, boolean isActive) {
                LogUtil.e(App.tag, "notificationOpened");
            }
        })
                .init();


    }

    public static App getApp() {
        return app;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getVerified() {
        return verified;
    }

    public void setVerified(int verified) {
        this.verified = verified;
    }

    public User getUser() {
        try {
            return (User) SharePersistent.getObjectValue(this, KEY_USER);
        } catch (Exception e) {
            LogUtil.e(App.tag, "get user error:" + e.getLocalizedMessage());
        }
        return null;

    }

    public void setUser(User user) {
        SharePersistent.setObjectValue(this, KEY_USER, user);
    }

    public static String getTag() {
        return tag;
    }

    public static String returnCacheDir() {
        firstRun();
        return CACHE_DIR;
    }

    public static void firstRun() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            CACHE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + DirFileName;
        } else {
            CACHE_DIR = Environment.getRootDirectory().getAbsolutePath() + "/" + DirFileName;
        }
        CACHE_DIR_PRODUCTS = CACHE_DIR + "/products";
        File cacheDir = new File(App.CACHE_DIR);
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        File cacheDirProducts = new File(App.CACHE_DIR_PRODUCTS);
        if (!cacheDirProducts.exists()) {
            cacheDirProducts.mkdirs();
        }
    }

    public void onPushReceive() {

    }


    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void putTemPObject(String key, Object object) {
        tempParamap.put(key, object);
    }


    public boolean hasTempKey(String key) {

        return tempParamap.containsKey(key);
    }


    public boolean containValidateBooleanKey(String valueKey) {

        if (hasTempKey(valueKey) && (boolean) getTempObject(valueKey) == true) {

            return true;
        }

        return false;
    }


    public void clearTempKey(String key) {
        if (tempParamap.containsKey(key)) {
            tempParamap.remove(key);
        }

    }

    public Object getTempObject(String key) {

        return tempParamap.remove(key);
    }

    public boolean isMainCreated() {

        return isMaincreate;
    }

    public void setMaincreate(boolean maincreate) {
        isMaincreate = maincreate;
    }

    public boolean isMainFront() {

        return isMainFront;
    }

    public void setIslgout(boolean isLogout) {
        SharePersistent.saveBoolean(this, "isLogout", isLogout);
    }

    public boolean isLogout() {
        return SharePersistent.getBoolean(this, "isLogout", false);
    }
}
