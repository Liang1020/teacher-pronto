package com.teacherpronto.android.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.view.RoundImageView;
import com.squareup.picasso.Picasso;
import com.teacherpronto.android.App;
import com.teacherpronto.android.R;
import com.teacherpronto.android.activity.BaseTeacherActivity;
import com.teacherpronto.android.activity.LoginActivity;
import com.teacherpronto.android.activity.MainActivity;
import com.teacherpronto.android.activity.VerificationActivity;
import com.teacherpronto.android.bean.User;
import com.teacherpronto.android.net.NetInterface;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by clia on 2016/4/25.
 */
public class ProfilePage extends BasePage {
    private static final String TAG = "PROFILEPAGE";

    public static final int VERIFICATION = 1;

    private User user;
    private TextView tvName;
    private TextView tvEmail;
    private TextView tvRegNum;
    private TextView tvPostCode;
    private TextView tvVerify;
    private TextView textViewPostCode;
    private TextView textViewDesc;
    private TextView textViewFormLoad;
    private TextView textViewRegistType;
    private TextView textViewRegNum;
    private TextView textViewRegistState;
    private TextView textViewPhoneNumber;
    private TextView textViewTeachingPpreference;
    private TextView textViewTeachSubjects;
    private TextView textViewTeachingyears;
    private TextView textViewTaughtYearleves;

    private RoundImageView roundImageView;

    private Animation animationFadeIn;
    private Animation animationFadeOut;

    public ProfilePage(final MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.rootView = View.inflate(mainActivity, R.layout.page_profile_details, null);

        animationFadeIn = AnimationUtils.loadAnimation(mainActivity, R.anim.abc_fade_in);
        animationFadeOut = AnimationUtils.loadAnimation(mainActivity, R.anim.abc_fade_out);

        animationFadeIn.setDuration(200);
        animationFadeOut.setDuration(200);

        initView();

    }

    private void initView() {
        tvName = findTextViewById(R.id.textViewName);
        tvEmail = findTextViewById(R.id.textViewEmail);
        tvRegNum = findTextViewById(R.id.textViewRegNum);
        tvPostCode = findTextViewById(R.id.textViewPostCode);
        tvVerify = findTextViewById(R.id.textViewVerify);
        roundImageView = (RoundImageView) findViewById(R.id.roundImageViewProfile);
        textViewPostCode = findTextViewById(R.id.textViewPostCode);
        textViewDesc = findTextViewById(R.id.textViewDesc);
        textViewFormLoad = findTextViewById(R.id.textViewFormLoad);
        textViewRegistType = findTextViewById(R.id.textViewRegistType);
        textViewRegistState = findTextViewById(R.id.textViewRegistState);
        textViewTeachingPpreference = findTextViewById(R.id.textViewTeachingPpreference);

        textViewPhoneNumber = findTextViewById(R.id.textViewPhoneNumber);
        textViewTeachSubjects = findTextViewById(R.id.textViewTeachSubjects);
        textViewTeachingyears = findTextViewById(R.id.textViewTeachingyears);

        textViewTaughtYearleves = findTextViewById(R.id.textViewTaughtYearleves);


        tvVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!user.isVerify()) {

                    BaseTeacherActivity.sendVerificationQuality(mainActivity);

                    App.getApp().putTemPObject("isfromProfile", true);
                    Tool.startActivityForResult(mainActivity, VerificationActivity.class, VERIFICATION);
                }
            }
        });


        user = App.getApp().getUser();
        if (null != user) {
            initUserView(user);
        } else {
            requestMe();
        }


    }

    @Override
    public void onShow() {
        //You can do something on this callback method,when main activity show this page,eg:refresh user info
        refreshAvatar();

    }

    @Override
    public void onResume() {
        refreshAvatar();
    }

    private void requestMe() {

        mainActivity.reSetTask();

        mainActivity.baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                baseTask.showDialogForSelf(true);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = null;
                try {
                    netResult = NetInterface.getProfile(mainActivity, NetInterface.buildCustomizedObject(paramMap));
                    user = (User) netResult.getData()[0];
                    App.getApp().setUser(user);//save user to local
                } catch (Exception e) {
                    LogUtil.e(TAG, "error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                baseTask.hideDialogSelf();
                if (null != result) {
                    if (result.isOk()) {
                        initUserView(user);
                    } else {
                        Tool.startActivity(mainActivity, LoginActivity.class);
                        Tool.showMessageDialog(result.getMessage(), mainActivity);
                    }

                } else {
                    Tool.showMessageDialog(R.string.error_net, mainActivity);
                }

            }
        }, mainActivity);

        HashMap<String, String> hmap = new HashMap<String, String>();

//        hmap.put("api_token", App.getApp().getToken());//NetInterface 中已经自动穿参数了

        mainActivity.baseTask.execute(hmap);

    }

    private void initUserView(User user) {

        if (null == user) {
            return;
        }


        tvVerify.setText(user.isVerify() ? "Verified" : "Verify");


        String forload = "1".equals(user.getForm_lodged()) ? "YES" : "NO";
        textViewFormLoad.setText(String.valueOf(forload));


        String registerType = user.getRegister_type();


        if (!StringUtils.isEmpty(registerType)) {
            try {
                String[] typeArray = mainActivity.getResources().getStringArray(R.array.profile_registed);
                int index = Integer.parseInt(registerType) - 1;
                String type = typeArray[index];
                textViewRegistType.setText(type);
            } catch (Exception e) {
            }
        }


        String registerstate = user.getRegister_state();

        if (!StringUtils.isEmpty(registerstate)) {
            try {
                String[] typeArrayState = mainActivity.getResources().getStringArray(R.array.profile_registed_state);
                textViewRegistState.setText(typeArrayState[Integer.parseInt(registerstate) - 1]);
            } catch (Exception e) {
            }
        }

        String teachPreference = user.getTeaching_preference();

        if (!StringUtils.isEmpty(teachPreference)) {
            if ("1".equals(teachPreference)) {
                textViewTeachingPpreference.setText("Primary");
            } else if ("2".equals(teachPreference)) {
                textViewTeachingPpreference.setText("Secondary");
            } else if ("3".equals(teachPreference)) {
                textViewTeachingPpreference.setText("P-12");
            } else if ("4".equals(teachPreference)) {
                textViewTeachingPpreference.setText("Other");
            }
        }


        ArrayList<String> subjects = user.getSubjects_preference();

        StringBuffer sbf = new StringBuffer();
        for (int i = 0, isize = subjects.size(); i < isize; i++) {
            if (i != isize - 1) {
                sbf.append(subjects.get(i) + ",");
            } else {
                sbf.append(subjects.get(i));
            }
        }

        textViewTeachSubjects.setText(sbf);
        //==================

        String yearExperences = user.getYear_experience();
        if (!StringUtils.isEmpty(yearExperences)) {
            String[] yearLevesl = mainActivity.getResources().getStringArray(R.array.profile_number_teacher_years);
            textViewTeachingyears.setText(yearLevesl[Integer.parseInt(yearExperences) - 1] + " years");
        }
        //==================


        ArrayList<String> levelexpers = user.getLevel_experience();

        if (!ListUtiles.isEmpty(levelexpers)) {

            if (ListUtiles.getListSize(levelexpers) == 1) {

                if (levelexpers.get(0).equals("15")) {
                    textViewTaughtYearleves.setText("Universify Graduate");
                } else if (levelexpers.get(0).equals("1")) {
                    textViewTaughtYearleves.setText("Prep/Kindergarten,Yr.1,Yr.2,Yr.3,Yr.4,Yr.5,Yr.6");
                } else {

                    String taught = "Yr." + String.valueOf(Integer.parseInt(levelexpers.get(0)) - 2);
                    textViewTaughtYearleves.setText(year2Text(taught));

                }
            } else {

                StringBuffer levelbuf = new StringBuffer();
                for (int i = 0, isize = levelexpers.size(); i < isize; i++) {
                    if (i != (isize - 1)) {
                        levelbuf.append(year2Text("Yr." + (Integer.parseInt(levelexpers.get(i)) - 2)) + ",");
                    } else {
                        levelbuf.append(year2Text("Yr." + (Integer.parseInt(levelexpers.get(i)) - 2)));
                    }
                }
                textViewTaughtYearleves.setText(levelbuf);
            }

        }


        //==================
        tvName.setText(user.getFirst_name() + " " + user.getLast_name());
        tvEmail.setText(user.getEmail());
        tvRegNum.setText(user.getRegister_number());

        textViewPostCode.setText(user.getPost_code());
        textViewDesc.setText(user.getIndroduction());
        textViewPhoneNumber.setText(user.getPhone());

        refreshAvatar();

    }

    private String year2Text(String input) {

        if ("Yr.0".equals(input)) {
            return "Prep/Kindergarten";
        } else {

            return input;
        }


    }


    public void refreshAvatar() {

        User user = App.getApp().getUser();
        if (null != user) {
            String avatarUrr = user.getAvatar();
            LogUtil.e(App.tag, "header avatar:" + avatarUrr);

            if (!StringUtils.isEmpty(avatarUrr)) {
                Picasso.with(App.getApp()).load(avatarUrr)
                        .config(Bitmap.Config.RGB_565)
                        .placeholder(R.drawable.bg_header_default)
                        .into(roundImageView);
            }
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == mainActivity.SHORT_GO_EDIT && resultCode == Activity.RESULT_OK) {
            initUserView(App.getApp().getUser());
        }

        if (requestCode == VERIFICATION && resultCode == Activity.RESULT_OK) {
            requestMe();
        }
    }
}
